
// Configuracion para el simulador Pendulo 

var simuladorParabolico   = new Object();

simuladorParabolico.textoSituacion1  = "msj_simuladorParabolicoSituacion1";

simuladorParabolico.textoSituacion2  = "msj_simuladorParabolicoSituacion2";


// Configuracion para el simulador Pendulo 

var simuladorPendulo   = new Object();

simuladorPendulo.textoSituacion1  = "msj_simuladorParabolicoSituacion1";

simuladorPendulo.textoSituacion2  = "msj_simuladorParabolicoSituacion2";



// Configuracion para el simulador Fluidos 

var simuladorFluidos = new Object();

simuladorFluidos.textoSituacion1  = "msj_simuladorParabolicoSituacion1";

simuladorFluidos.textoSituacion2  = "msj_simuladorParabolicoSituacion2";


// Escribir preguntas parabolico 

var preguntasParabolico = new Object();

preguntasParabolico.textoPregunta1  = "¿El tiro parabólico se considera un movimiento en el plano?";

preguntasParabolico.textoPregunta2  = "¿En un tiro parabólico la velocidad resultante resulta de combinar Vx, Vy y Vz?";

preguntasParabolico.textoPregunta3  = "¿Un movimiento bidimensional es aquel que está sometido a un movimiento horizontal y a otro vertical?";

preguntasParabolico.textoPregunta4 = "¿El movimiento parabólico es la composición de dos movimientos rectilíneos?";

preguntasParabolico.textoPregunta5 = "Un cuerpo lanzado verticalmente hacia arriba y otro parabólicamente completo que alcance la misma altura no tardan lo mismo en caer.";

//Escribir preguntas pendulo

var preguntasPendulo = new Object();

preguntasPendulo.textoPregunta1  = "El movimiento pendular se considera un movimiento armónico simple";

preguntasPendulo.textoPregunta2  = "Graficamente el movimiento pendular está representado por una gráfica triangular";

preguntasPendulo.textoPregunta3  = "Si se tienen 2 péndulos de igual longitud y masa, con diferentes amplitudes de recorrido mayor, el periodo de estos péndulos es el mismo.";

preguntasPendulo.textoPregunta4 = "El periodo de un péndulo es directamente proporcional a la raíz cuadrada de su masa.";

preguntasPendulo.textoPregunta5 = "Un pédulo describe una trayectoria circular.";

//Escribir preguntas fluidos

var preguntasFluidos = new Object();

preguntasFluidos.textoPregunta1  = "pregunta1";

preguntasFluidos.textoPregunta2  = "pregunta2";

preguntasFluidos.textoPregunta3  = "pregunta3";

preguntasFluidos.textoPregunta4 = "pregunta4";

preguntasFluidos.textoPregunta5 = "pregunta5";