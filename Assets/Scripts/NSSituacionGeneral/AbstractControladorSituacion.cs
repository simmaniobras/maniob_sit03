using UnityEngine;

namespace NSSituacionGeneral
{
    public abstract class AbstractControladorSituacion : MonoBehaviour
    {
        #region members

        protected bool cortaCircuitosAbiertos;
        
        #endregion

        #region accesores

        public bool CortaCircuitosAbiertos
        {
            get { return cortaCircuitosAbiertos; }
        }

        #endregion
    }
}