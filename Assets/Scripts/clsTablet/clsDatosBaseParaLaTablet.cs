﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Tablet/Casos")]
/// clase que alberga los elementos necesario para la creacion de un caso en al tablet  
public class clsDatosBaseParaLaTablet : ScriptableObject
{
    #region members

    [SerializeField] private float[] tramoEnMetros;

    [SerializeField] private float[] NumeroDeUsuarios;

    [SerializeField] private float ConstanteNKVA_MAX_USUARIO;

    private float[] corrienteA;

    private float[] kvaTotales;

    private float[] momentoElectrico;

    private float[] parcial;

    [SerializeField] private float Voltaje_Fase_neutro_BT;

    private float[] tension_de_recibo;

    private float[] tension_de_envio;

    private float[] Regulacion_tension_acumulada;

    #endregion

    #region accesors

    public float[] _tramoEnMetros
    {
        get { return tramoEnMetros; }
    }

    public float[] _NumeroDeUsuarios
    {
        get { return NumeroDeUsuarios; }
    }

    public float[] _KvaTotalesTramo
    {
        get { return kvaTotales; }
    }

    public float[] _momentoElectrico
    {
        get { return momentoElectrico; }
    }

    public float[] _tension_de_recibo
    {
        get { return tension_de_recibo; }
    }

    public float[] _tension_de_envio
    {
        get { return tension_de_envio; }
    }

    public float[] _corrienteA
    {
        get { return corrienteA; }
    }

    public float[] _Regulacion_tension_acumulada
    {
        get { return Regulacion_tension_acumulada; }
    }

    #endregion

    #region public methods

    /// <summary>
    /// genera todas las columnas de la tablet 
    /// </summary>
    /// <param name="contantesDeRegualcionDelCableElejido"></param>
    public void calcularDatosDetabla(float[] contantesDeRegualcionDelCableElejido)
    {
        CalcularKvaTotalesTramo();
        CalcularMomentoElectrico();
        CalcularParcial(contantesDeRegualcionDelCableElejido);
        CalcularTensionRecibo();
        CalcularCorriente();
        CalcularTensionAcumulada();
    }

    #endregion


    #region private methods

    private void CalcularKvaTotalesTramo()
    {
        kvaTotales = new float[NumeroDeUsuarios.Length];

        for (int i = 0; i < NumeroDeUsuarios.Length; i++)
            kvaTotales[i] = (NumeroDeUsuarios[i] * ConstanteNKVA_MAX_USUARIO) + 2f;
    }

    private void CalcularMomentoElectrico()
    {
        momentoElectrico = new float[tramoEnMetros.Length];

        for (int i = 0; i < tramoEnMetros.Length; i++)
            momentoElectrico[i] = tramoEnMetros[i] * kvaTotales[i];
    }

    private void CalcularParcial(float[] ConstanteDelCableK)
    {
        parcial = new float[ConstanteDelCableK.Length];

        for (int i = 0; i < momentoElectrico.Length; i++)
            parcial[i] = (ConstanteDelCableK[i] * momentoElectrico[i]) / 1000f;
    }

    private void CalcularTensionRecibo()
    {
        tension_de_recibo = new float[momentoElectrico.Length];
        tension_de_envio = new float[momentoElectrico.Length];

        tension_de_envio[0] = Voltaje_Fase_neutro_BT;
        tension_de_recibo[0] = tension_de_envio[0] - ((tension_de_envio[0] * parcial[0]) / 100f);

        tension_de_envio[1] = tension_de_recibo[0];
        tension_de_recibo[1] = tension_de_envio[1] - ((tension_de_envio[1] * parcial[1]) / 100f);

        tension_de_envio[2] = tension_de_recibo[1];
        tension_de_recibo[2] = tension_de_envio[2] - ((tension_de_envio[2] * parcial[2]) / 100f);

        tension_de_envio[3] = tension_de_recibo[2];
        tension_de_recibo[3] = tension_de_envio[3] - ((tension_de_envio[3] * parcial[3]) / 100f);


        tension_de_envio[4] = tension_de_recibo[2];
        tension_de_recibo[4] = tension_de_envio[4] - ((tension_de_envio[4] * parcial[4]) / 100f);

        tension_de_envio[5] = tension_de_recibo[4];
        tension_de_recibo[5] = tension_de_envio[5] - ((tension_de_envio[5] * parcial[5]) / 100f);


        tension_de_envio[6] = Voltaje_Fase_neutro_BT;
        tension_de_recibo[6] = tension_de_envio[6] - ((tension_de_envio[6] * parcial[6]) / 100f);

        tension_de_envio[7] = tension_de_recibo[6];
        tension_de_recibo[7] = tension_de_envio[7] - ((tension_de_envio[7] * parcial[7]) / 100f);

        tension_de_envio[8] = tension_de_recibo[7];
        tension_de_recibo[8] = tension_de_envio[8] - ((tension_de_envio[8] * parcial[8]) / 100f);


        tension_de_envio[9] = tension_de_recibo[6];
        tension_de_recibo[9] = tension_de_envio[9] - ((tension_de_envio[9] * parcial[9]) / 100f);

        tension_de_envio[10] = tension_de_recibo[9];
        tension_de_recibo[10] = tension_de_envio[10] - ((tension_de_envio[10] * parcial[10]) / 100f);

        tension_de_envio[11] = tension_de_recibo[10];
        tension_de_recibo[11] = tension_de_envio[11] - ((tension_de_envio[11] * parcial[11]) / 100f);
    }


    private void CalcularCorriente()
    {
        corrienteA = new float[tension_de_recibo.Length];
        
        for (int i = 0; i < momentoElectrico.Length; i++)
            corrienteA[i] = (kvaTotales[i] * 1000f) / (3f * tension_de_recibo[i]);
    }

    private void CalcularTensionAcumulada()
    {
        Regulacion_tension_acumulada = new float[tension_de_recibo.Length]; // posiciones 11
        Regulacion_tension_acumulada[0] = ((tension_de_envio[0] - tension_de_recibo[0]) * 100f) / tension_de_recibo[0];
        Regulacion_tension_acumulada[1] = ((tension_de_envio[1] - tension_de_recibo[1]) * 100f) / tension_de_recibo[1] + Regulacion_tension_acumulada[0];
        Regulacion_tension_acumulada[2] = ((tension_de_envio[2] - tension_de_recibo[2]) * 100f) / tension_de_recibo[2] + Regulacion_tension_acumulada[1];
        Regulacion_tension_acumulada[3] = ((tension_de_envio[3] - tension_de_recibo[3]) * 100f) / tension_de_recibo[3] + Regulacion_tension_acumulada[2];

        Regulacion_tension_acumulada[4] = ((tension_de_envio[4] - tension_de_recibo[4]) * 100f) / tension_de_recibo[4] + Regulacion_tension_acumulada[2];
        Regulacion_tension_acumulada[5] = ((tension_de_envio[5] - tension_de_recibo[5]) * 100f) / tension_de_recibo[5] + Regulacion_tension_acumulada[4];

        Regulacion_tension_acumulada[6] = ((tension_de_envio[6] - tension_de_recibo[6]) * 100f) / tension_de_recibo[6];
        Regulacion_tension_acumulada[7] = ((tension_de_envio[7] - tension_de_recibo[7]) * 100f) / tension_de_recibo[7] + Regulacion_tension_acumulada[6];
        Regulacion_tension_acumulada[8] = ((tension_de_envio[8] - tension_de_recibo[8]) * 100f) / tension_de_recibo[8] + Regulacion_tension_acumulada[7];

        Regulacion_tension_acumulada[9] = ((tension_de_envio[9] - tension_de_recibo[9]) * 100f) / tension_de_recibo[9] + Regulacion_tension_acumulada[6];
        Regulacion_tension_acumulada[10] = ((tension_de_envio[10] - tension_de_recibo[10]) * 100f) / tension_de_recibo[10] + Regulacion_tension_acumulada[9];
        Regulacion_tension_acumulada[11] = ((tension_de_envio[11] - tension_de_recibo[11]) * 100f) / tension_de_recibo[11] + Regulacion_tension_acumulada[10];
    }

    #endregion
}