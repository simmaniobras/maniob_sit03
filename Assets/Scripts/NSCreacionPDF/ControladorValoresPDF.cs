﻿using NSEvaluacion;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NSTraduccionIdiomas;
using TMPro;
using UnityEngine.Serialization;

namespace NSCreacionPDF
{
    /// <summary>
    /// Clase que contiene todos los textos UI y objetos UI del PDF que necesitan asignacion de valores con respecto a lo que sucedio en el simulador
    /// </summary>
    public class ControladorValoresPDF : MonoBehaviour
    {
        #region members
        public CanvasReportePDF mgPdf;

        [SerializeField, Header("Valores y datos del usuario"), Space(10)]
        private TextMeshProUGUI textUsuarioValor;

        [SerializeField] private TextMeshProUGUI textInstitucionValor;

        [SerializeField] private TextMeshProUGUI textSituacionValor;

        [SerializeField] private TextMeshProUGUI textCursoValor;

        [SerializeField] private TextMeshProUGUI textUnidadValor;

        [SerializeField] private TextMeshProUGUI textIDCursoValor;

        [SerializeField] private TextMeshProUGUI textFechaInicioValor;

        [SerializeField] private TextMeshProUGUI textTiempoPracticaValor;

        [SerializeField] private TextMeshProUGUI textIntentosValor;

        [SerializeField] private TextMeshProUGUI textCalificacionValor;

        /// <summary>
        /// transform padre donde deben ser creados los registros de datos
        /// </summary>
        [SerializeField, Header("Registro Datos situacion seleccionada"), Space(10)]
        private Transform contenedorRegistroDatos;

        [SerializeField] private GameObject PanelRegistroDatos;

        [SerializeField, Header("Preguntas evaluacion"), Space(10)]
        private Image imageEnunciadoEvaluacion;

        [SerializeField] private TextMeshProUGUI textEnunciadoEvaluacion;

        /// <summary>
        /// transform padre donde deben ser creadas las preguntas de evaluacion
        /// </summary>
        [SerializeField] private Transform transformPreguntasEvaluacion;

        /// <summary>
        /// transform padre donde deben ser creadas las respuestas del usuario 
        /// </summary>
        [SerializeField] private Transform transformListaRespuestasUsuario;

        /// <summary>
        /// transform padre en donde deben ser creados los numeros de las respuestas del usuario
        /// </summary>
        // [SerializeField] private Transform transformEnumeracionRespuestasUsuario;

        /// <summary>
        /// transform padre donde deben ser creadas las respuestas correctas de la evaluacion
        /// </summary>
        [SerializeField] private Transform transformListaRespuestasCorrectas;

        /// <summary>
        /// transform padre en donde deben ser creados los numeros de las respuestas correctas
        /// </summary>
        // [SerializeField] private Transform transformEnumeracionRespuestasCorrectas;

        [SerializeField] private GameObject prefabTextPreguntaRespuesta;

        // [SerializeField] private GameObject prefabTextEnumeracion;

        //[SerializeField] private GameObject prefabContenedorImageRespuesta;

        /*[SerializeField, Header("Preguntas cuaderno"), Space(10)]
        private GameObject prefabPreguntaCuaderno;*/

        [SerializeField] private TextMeshProUGUI textPreguntasCuaderno1;
        
        [SerializeField] private TextMeshProUGUI textPreguntasCuaderno2;

        #endregion

        #region public methods

        public void SetUsuario(string argUsuario)
        {
            textUsuarioValor.text = argUsuario;
        }

        public void SetInstitucion(string argInstitucion)
        {
            textInstitucionValor.text = argInstitucion;
        }

        public void SetSituacion(string argSituacion)
        {
            textSituacionValor.text = argSituacion;
        }

        public void SetCurso(string argCurso)
        {
            textCursoValor.text = argCurso;
        }

        public void SetUnidad(string argUnidad)
        {
            textUnidadValor.text = argUnidad;
        }

        public void SetIDCurso(string argIDCurso)
        {
            textIDCursoValor.text = argIDCurso;
        }

        public void SetFechaInicio(string argFechaInicio)
        {
            textFechaInicioValor.text = argFechaInicio;
        }

        public void SetTiempoPractica(string argTiempoPractica)
        {
            textTiempoPracticaValor.text = argTiempoPractica;
        }

        public void SetIntentos(string argIntentos)
        {
            textIntentosValor.text = argIntentos;
        }

        public void SetCalificacion(string argCalificacion)
        {
            textCalificacionValor.text = argCalificacion;
        }

        public void SetInputsRegistroDatos(GameObject argInputsRegistroDatos)
        {
            if (contenedorRegistroDatos.childCount == 1)
                Destroy(contenedorRegistroDatos.GetChild(0).gameObject);

            Instantiate(argInputsRegistroDatos, contenedorRegistroDatos);
        }

        public void SetPanelRegistroDatos()
        {
            var tmpPanelRegistroDatos = Instantiate(PanelRegistroDatos, contenedorRegistroDatos);
            tmpPanelRegistroDatos.transform.localScale = Vector2.one * 0.375f;
        }

        public void SetPreguntasSituacion(PreguntasSituacion argPreguntasSituacion, string[] argRespuestasUsuario, Sprite[] argRespuestasImagenUsuario)
        {
            //enunciado evaluacion
            mgPdf.panelesHojasPDF[1].SetActive(true);

            imageEnunciadoEvaluacion.sprite = argPreguntasSituacion._imagenEnunciado;
            textEnunciadoEvaluacion.text = DiccionarioIdiomas._instance.Traducir(argPreguntasSituacion._textoEnunciado);

            //borrar preguntas antiguas
            while (transformPreguntasEvaluacion.childCount > 1)
                DestroyImmediate(transformPreguntasEvaluacion.GetChild(1).gameObject);

            //asignar preguntas nuevas.
            for (int i = 0; i < argPreguntasSituacion._preguntas.Length; i++)
            {
                var tmpTextPreguntaEvaluacion = Instantiate(prefabTextPreguntaRespuesta, transformPreguntasEvaluacion).GetComponent<TextMeshProUGUI>();
                tmpTextPreguntaEvaluacion.text = DiccionarioIdiomas._instance.Traducir(argPreguntasSituacion._preguntas[i]);
            }

            LayoutRebuilder.ForceRebuildLayoutImmediate(transformPreguntasEvaluacion.GetComponent<RectTransform>()); 
            //borrar respuestas correctas antiguas
            while (transformListaRespuestasCorrectas.childCount > 0)
                DestroyImmediate(transformListaRespuestasCorrectas.GetChild(0).gameObject);

            //asignar respuestas correctas nuevas.
            for (int i = 0; i < argPreguntasSituacion._respuestas.Length; i++)
            {
                var tmpTextRespuestaCorrectaEvaluacion = Instantiate(prefabTextPreguntaRespuesta, transformListaRespuestasCorrectas).GetComponent<TextMeshProUGUI>();
                tmpTextRespuestaCorrectaEvaluacion.text = DiccionarioIdiomas._instance.Traducir(argPreguntasSituacion._respuestas[i]);
            }
            
            LayoutRebuilder.ForceRebuildLayoutImmediate(transformListaRespuestasCorrectas.GetComponent<RectTransform>());
            //borrar respuestas usuario antiguas
            while (transformListaRespuestasUsuario.childCount > 0)
                DestroyImmediate(transformListaRespuestasUsuario.GetChild(0).gameObject);

            //asignar respuestas usuario nuevas.
            for (int i = 0; i < argPreguntasSituacion._contenedorRespuestasFalsas.Length; i++)
            {
                var tmpTextRespuestaCorrectaEvaluacion = Instantiate(prefabTextPreguntaRespuesta, transformListaRespuestasUsuario).GetComponent<TextMeshProUGUI>();
                tmpTextRespuestaCorrectaEvaluacion.text =  argRespuestasUsuario[i];
            }
            
            LayoutRebuilder.ForceRebuildLayoutImmediate(transformListaRespuestasUsuario.GetComponent<RectTransform>());
            mgPdf.panelesHojasPDF[1].SetActive(false);
        }

        public void SetPreguntasCuaderno(string[] argPreguntasCuaderno, bool argHanEscritoRespuestas)
        {
            textPreguntasCuaderno1.text = "";
            textPreguntasCuaderno2.text = "";

            Debug.Log(argPreguntasCuaderno[1]);
            
            for (int i = 0; i < argPreguntasCuaderno.Length; i++)
                textPreguntasCuaderno2.text += argPreguntasCuaderno[i] + "\n\n";
        }

        #endregion
    }
}