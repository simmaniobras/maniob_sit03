using NSSeguridad;
using System.Collections.Generic;
using System.Xml;
using NSScore;
using UnityEngine;

namespace NSEvaluacion
{
    public class Evaluacion : MonoBehaviour
    {
        #region members

        /// <summary>
        /// Rango de calificacion con posibilidad de definicion, ejemplo, hasta 5 cuando el array es definido con un solo elemento y este es numerico, ó de (5 - 10) 
        /// Cuando el array es definido con 2 elementos numericos, siendo el numero en el indice 0 la la nota mas baja y el numero en el indice 1 la nota mas alta 
        /// ó A - B - C - D - E si los elementos del array son definidos como letras, minimo 2 letras
        /// </summary>
        [SerializeField] private string[] rangoCalificacion;

        /// <summary>
        /// Rango de porcentaje para los intervalos
        /// </summary>
        [SerializeField] private float[] rangoPorcentaje5Letras;

        /// <summary>
        /// Clase que contiene las calificaciones de la situacion actualmente ejecutandose, esta clase se crea cada vez que se entra en una situación.
        /// </summary>
        [SerializeField] private CalificacionSituacion refCalificacionSituacion;

        /// <summary>
        /// Clase que contiene las calificaciones de la situacion actualmente ejecutandose, esta clase se crea cada vez que se entra en una situación.
        /// </summary>
        private CalificacionSituacion2 refCalificacionSituacion2;

        private CalificacionSituacion3 refCalificacionSituacion3;

        private CalificacionSituacion4 refCalificacionSituacion4;

        /// <summary>
        /// Envio PDF
        /// </summary>
        [SerializeField] private clsEnvioPdf EnvioPdf;

        [SerializeField] private int NumeroPractica;

        #endregion

        #region accesors

        public CalificacionSituacion _refCalificacionSituacion
        {
            get { return refCalificacionSituacion; }
        }

        public CalificacionSituacion2 _refCalificacionSituacion2
        {
            get { return refCalificacionSituacion2; }
        }

        public CalificacionSituacion3 _refCalificacionSituacion3
        {
            get { return refCalificacionSituacion3; }
        }

        public CalificacionSituacion4 _refCalificacionSituacion4
        {
            get { return refCalificacionSituacion4; }
        }

        #endregion

        private void Awake()
        {
            NuevaCalificacion();
        }

        #region public methods

        /// <summary>
        /// Traduce la calificacion total de la situacion actual a la calificacion que le corresponde en el rango definido
        /// </summary>
        public string GetCalificacionTotalRango()
        {
            string tmpCalificacionFinal = "";

            switch (NumeroPractica)
            {
                case 1:
                    tmpCalificacionFinal = FindObjectOfType<ScoreController>().GetScoreFromFactor01(refCalificacionSituacion.GetCalificacionTotal());
                    break;

                case 2:
                    tmpCalificacionFinal = FindObjectOfType<ScoreController>().GetScoreFromFactor01(refCalificacionSituacion2.GetCalificacionTotal());
                    break;
                
                case 3:
                    tmpCalificacionFinal = FindObjectOfType<ScoreController>().GetScoreFromFactor01(refCalificacionSituacion3.GetCalificacionTotal());
                    break;
                
                case 4:
                    tmpCalificacionFinal = FindObjectOfType<ScoreController>().GetScoreFromFactor01(refCalificacionSituacion4.GetCalificacionTotal());
                    break;
            }

            return tmpCalificacionFinal;
        }

        public void AsiganarCalidicacionTraformadorCorrecto(float argCalificacion)
        {
            refCalificacionSituacion4._seleccionCorrectaTransformador = argCalificacion;
        }
        
        public void AsignarCalificacionFusibleSeleccioando(float argCalificacion)
        {
            switch (NumeroPractica)
            {
                case 1:
                    refCalificacionSituacion._fusibleSeleccionadoCorrecto = argCalificacion;
                    Debug.Log("AsignarCalificacionFusibleSeleccioando : " + argCalificacion);
                    break;
                case 4:
                    refCalificacionSituacion4._fusibleSeleccionadoCorrecto = argCalificacion;
                    Debug.Log("AsignarCalificacionFusibleSeleccioando : " + argCalificacion);
                    break;
            }
        }

        public void AsignarCalificacionRegistroDatos(float argCalificacion)
        {
            //refCalificacionSituacion._registroDatosCorrectos = argCalificacion;
            switch (NumeroPractica)
            {
                case 1:
                    refCalificacionSituacion._registroDatosCorrectos = argCalificacion;
                    break;

                case 2:
                    refCalificacionSituacion2._registroDatosCorrectos = argCalificacion;
                    break;
                case 3:
                    refCalificacionSituacion3._registroDatosCorrectos = argCalificacion;
                    break;
                case 4:
                    refCalificacionSituacion4._registroDatosCorrectos = argCalificacion;
                    break;
            }

            Debug.Log("AsignarRegistroDatosCorrectos : " + argCalificacion);
        }

        public void AsignarCalificacionPreguntasEvaluacion(float argCalificacion)
        {
            //  refCalificacionSituacion._preguntasEvaluacionCorrectas = argCalificacion;
            switch (NumeroPractica)
            {
                case 1:
                    refCalificacionSituacion._preguntasEvaluacionCorrectas = argCalificacion;
                    break;

                case 2:
                    refCalificacionSituacion2._preguntasEvaluacionCorrectas = argCalificacion;
                    break;

                case 3:
                    refCalificacionSituacion3._preguntasEvaluacionCorrectas = argCalificacion;
                    break;
                case 4:
                    refCalificacionSituacion4._preguntasEvaluacionCorrectas = argCalificacion;
                    break;
            }

            Debug.Log("AsignarCalificacionPreguntasEvaluacion : " + argCalificacion);
        }

        public void AsignarCalificacionElementosProteccion(float argCalificacion)
        {
            //refCalificacionSituacion._seleccionCorrectaElementosProteccion = argCalificacion;
            switch (NumeroPractica)
            {
                case 1:
                    refCalificacionSituacion._seleccionCorrectaElementosProteccion = argCalificacion;
                    break;

                case 2:
                    refCalificacionSituacion2._seleccionCorrectaElementosProteccion = argCalificacion;
                    break;
                case 3:
                    refCalificacionSituacion3._seleccionCorrectaElementosProteccion = argCalificacion;
                    break;
            }

            Debug.Log("AsignarCalificacionElementosProteccion : " + argCalificacion);
        }

        public void AsignarCalificacionElementosZonaSeguridad(float argCalificacion)
        {
            //refCalificacionSituacion._calificacionZonaSeguridad = argCalificacion;
            switch (NumeroPractica)
            {
                case 1:
                    refCalificacionSituacion._calificacionZonaSeguridad = argCalificacion;
                    break;

                case 2:
                    refCalificacionSituacion2._calificacionZonaSeguridad = argCalificacion;
                    break;

                case 3:
                    refCalificacionSituacion3._calificacionZonaSeguridad = argCalificacion;
                    break;
            }
        }

        public void AsignarCalificacionMensajesEnviados(float argCalificacion)
        {
            //refCalificacionSituacion._calificacionMensajesEnviados = argCalificacion;
            switch (NumeroPractica)
            {
                case 1:
                    refCalificacionSituacion._calificacionMensajesEnviados = argCalificacion;
                    break;

                case 2:
                    refCalificacionSituacion2._calificacionMensajesEnviados = argCalificacion;
                    break;
                case 3:
                    //refCalificacionSituacion3._calificacionMensajesEnviados = argCalificacion;
                    break;
            }

            Debug.Log("AsignarCalificacionMensajesEnviados : " + argCalificacion);
        }

        public void AsignarCantidadIntentos(float argIntentos)
        {
            //refCalificacionSituacion._cantidadIntentos = argIntentos;
            switch (NumeroPractica)
            {
                case 1:
                    refCalificacionSituacion._cantidadIntentos = argIntentos;
                    break;

                case 2:
                    refCalificacionSituacion2._cantidadIntentos = argIntentos;
                    break;
                case 3:
                    refCalificacionSituacion3._cantidadIntentos = argIntentos;
                    break;
                case 4:
                    refCalificacionSituacion4._cantidadIntentos = argIntentos;
                    break;
            }

            Debug.Log("AsignarCantidadIntentos : " + argIntentos);
        }

        /// <summary>
        /// se ingresa el valor de la calificaicon dad al ejecutar lso fusibles 1 si se realiza  
        /// </summary>
        /// <param name="argFusibles"></param>
        public void AsignarCalificaiconAperturaDeFusibles(float argFusibles)
        {
            refCalificacionSituacion3._CalificaiconAperturaDeFusibles = argFusibles;
        }


        /// <summary>
        /// se ingresa la calificacion optenida el poner el letrero de no operar(1 si lo hace )
        /// </summary>
        /// <param name="argFusibles"></param>
        public void AsignarCalificacionUsoNoOperar(float argFusibles)
        {
            refCalificacionSituacion3._CalificacionUsoNoOperar = argFusibles;
        }

        /// <summary>
        /// se ingresa la calificaicon de la maniobra de puesta a tierra(1 si la hace)
        /// </summary>
        /// <param name="argFusibles"></param>
        public void AsignarCalificacionUsoTierraPortable(float argFusibles)
        {
            refCalificacionSituacion3._CalificacionUsoTierraPortable = argFusibles;
        }


        /// <summary>
        /// Crea un objeto de calificacion nuevo, esto debe usarse cada vez que el usuario selecciona una situacion
        /// </summary>
        public void NuevaCalificacion()
        {
            switch (NumeroPractica)
            {
                case 1:
                    refCalificacionSituacion = new CalificacionSituacion();
                    break;

                case 2:
                    refCalificacionSituacion2 = new CalificacionSituacion2();
                    break;

                case 3:
                    refCalificacionSituacion3 = new CalificacionSituacion3();
                    break;

                case 4:
                    refCalificacionSituacion4 = new CalificacionSituacion4();
                    break;
            }
        }

        #endregion

        #region private methods

        private void CargarNotasDesdeXML()
        {
#if !UNITY_WEBGL
            var tmpXmlDocument = new XmlDocument();

            try
            {
                tmpXmlDocument.Load(ClsSeguridad._instance._fileName);
                var tmpListRangos = new List<string>();

                XmlNodeList tmpXmlNodeList = tmpXmlDocument.DocumentElement.GetElementsByTagName("rango");

                for (int i = 0; i < tmpXmlNodeList.Count; i++)
                    tmpListRangos.Add(tmpXmlNodeList[i].InnerXml);

                if (tmpListRangos.Count > 0)
                    rangoCalificacion = tmpListRangos.ToArray();

                Debug.LogError(rangoCalificacion.Length + "length" + tmpXmlNodeList.Count);
            }
            catch (System.Exception e)
            {
                Debug.Log("No se encontro el archivo XML, se usaran las notas por default|| Excepcion : " + e);
            }
#endif
        }

        #endregion
    }
}