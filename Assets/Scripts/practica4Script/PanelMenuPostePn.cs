﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NSInterfaz;
using NSBoxMessage;
using NSTraduccionIdiomas;
using UnityEngine.UI;
using NSEvaluacion;
using NSSituacion3;


public class PanelMenuPostePn : AbstractSingletonPanelUIAnimation<PanelInterfazGeneralSituacion2>
{
    [SerializeField]
    public ObjTrafo[] ObjetosDelTrasformador;

    public ControladorSituacion4 ControlSit4;

    [SerializeField]
    private GameObject[] cables;

    public PanelPostePn panelPn;

    [SerializeField]
    private Button BotonMenuDeTransformador;
    
    [SerializeField]
    private Button[] BotonesDelMenu;

    private Dictionary<ObjTrafo.tipoElemento, int> objetosMontadosEnELTrafo;

    private ObjTrafo objTrafoActual;

    private int idBotonActual;

    private int NumEstadoActualSecuencia;

    private bool pernoActivo;

    private int ObjetosConperno;

    [SerializeField]
    private Toggle perno;

    // Use this for initialization
    void Start() {
        objetosMontadosEnELTrafo = new Dictionary<ObjTrafo.tipoElemento, int>();
        objTrafoActual = new ObjTrafo();
        NumEstadoActualSecuencia = -1;
        ObjetosConperno = 0;
    }

    // Update is called once per frame
    void Update() {

    }

    /// <summary>
    ///  Se llama una vez se ubique el objeto para guardar el id de este
    /// </summary>
    public void objUbicado()
    {
        if (objTrafoActual.TodosLosOjetoPuesto())
        { 
            objetosMontadosEnELTrafo.Add(objTrafoActual.NombreDelElemento, idBotonActual);
            BotonesDelMenu[idBotonActual].interactable=false;
            BotonMenuDeTransformador.interactable = true;
        }
        Debug.Log("Numero de elementos"+objetosMontadosEnELTrafo.Count);

        if (objetosMontadosEnELTrafo.Count == ObjetosDelTrasformador.Length)
        {
            BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("TextMontajeCOmpletado"), "ACEPTAR");
            BotonMenuDeTransformador.interactable = false;
            visualizarCables(true);
            ControlSit4._trasnformadorarmadoInstaldo = true;
            ControlSit4.calificarTrafoYFusible();
        }
        /*if (ObjetosConperno == 2)
            perno.interactable = false;*/

    }

    public void reiniciar()
    {
        BotonMenuDeTransformador.interactable = true;
        objetosMontadosEnELTrafo.Clear();
        visualizarCables(false);
        ControlSit4._trasnformadorarmadoInstaldo = false;
    }


    public void ObjSeleccionadoIsValido(int IdObjetoSeleccionado)
    {
        desactivarSeleccion();
        if (IdObjetoSeleccionado < ObjetosDelTrasformador.Length)
        {
            switch (ObjetosDelTrasformador[IdObjetoSeleccionado].NombreDelElemento)
            {
                case ObjTrafo.tipoElemento.aisaldores:
                    if (objetosMontadosEnELTrafo.ContainsKey(ObjTrafo.tipoElemento.cruceta) && objetosMontadosEnELTrafo.ContainsKey(ObjTrafo.tipoElemento.diagonales))
                        activarPiezaTrafo(IdObjetoSeleccionado);
                    else
                        BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("textObjetoSeleccionado"), "ACEPTAR");

                    break;

                case ObjTrafo.tipoElemento.aisaldoresCarrete:
                    if (objetosMontadosEnELTrafo.ContainsKey(ObjTrafo.tipoElemento.cintaMetalica))
                        activarPiezaTrafo(IdObjetoSeleccionado);
                    else
                        BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("textObjetoSeleccionado"), "ACEPTAR");

                    break;

                case ObjTrafo.tipoElemento.cintaMetalica:
                    if (objetosMontadosEnELTrafo.ContainsKey(ObjTrafo.tipoElemento.SoporteAisladorCarrete))
                        activarPiezaTrafo(IdObjetoSeleccionado);
                    else
                        BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("textObjetoSeleccionado"), "ACEPTAR");

                    break;

                case ObjTrafo.tipoElemento.collarines:
                    ObjetosDelTrasformador[IdObjetoSeleccionado].ActiveParteTrafo(true);
                    objTrafoActual = ObjetosDelTrasformador[IdObjetoSeleccionado];
                    Mostrar(false);
                    break;

                case ObjTrafo.tipoElemento.cortacircuitos:
                    if (objetosMontadosEnELTrafo.ContainsKey(ObjTrafo.tipoElemento.cruceta) && objetosMontadosEnELTrafo.ContainsKey(ObjTrafo.tipoElemento.diagonales))
                        activarPiezaTrafo(IdObjetoSeleccionado);
                    else
                        BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("textObjetoSeleccionado"), "ACEPTAR");

                    break;

                case ObjTrafo.tipoElemento.cruceta:
                    if (objetosMontadosEnELTrafo.ContainsKey(ObjTrafo.tipoElemento.collarines))
                      /*  if (pernoActivo)
                        {*/
                            activarPiezaTrafo(IdObjetoSeleccionado);
                            //ObjetosConperno++;
                    /*    }
                        else
                        {
                            BoxMessageManager._instance.MtdCreateBoxMessageInfo("El Objeto Seleccionado Necesita del perno", "ACEPTAR");
                        }
                    else
                        BoxMessageManager._instance.MtdCreateBoxMessageInfo("El Objeto Seleccionado aun no se puede ubicar.", "ACEPTAR");
                    */
                    break;

                case ObjTrafo.tipoElemento.diagonales:
                    if (objetosMontadosEnELTrafo.ContainsKey(ObjTrafo.tipoElemento.collarines))
                       /* if (pernoActivo)
                        {*/
                            activarPiezaTrafo(IdObjetoSeleccionado);
                            //ObjetosConperno++;
                       /* }
                        else
                        {
                            BoxMessageManager._instance.MtdCreateBoxMessageInfo("El Objeto Seleccionado Necesita del perno", "ACEPTAR");
                        }*/
                    else
                        BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("textObjetoSeleccionado"), "ACEPTAR");


                    break;

                case ObjTrafo.tipoElemento.dps:
                    if (objetosMontadosEnELTrafo.ContainsKey(ObjTrafo.tipoElemento.transformador))
                        activarPiezaTrafo(IdObjetoSeleccionado);
                    else
                        BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("textObjetoSeleccionado"), "ACEPTAR");

                    break;

                case ObjTrafo.tipoElemento.transformador:
                    if (objetosMontadosEnELTrafo.ContainsKey(ObjTrafo.tipoElemento.collarines))
                        activarPiezaTrafo(IdObjetoSeleccionado);
                    else
                        BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("textObjetoSeleccionado"), "ACEPTAR");

                    break;
                case ObjTrafo.tipoElemento.SoporteAisladorCarrete:
                    if (objetosMontadosEnELTrafo.ContainsKey(ObjTrafo.tipoElemento.collarines))
                        activarPiezaTrafo(IdObjetoSeleccionado);
                    else
                        BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("textObjetoSeleccionado"), "ACEPTAR");
                    break;
                    /* case ObjTrafo.tipoElemento.Perno:
                         if (objetosMontadosEnELTrafo.ContainsKey(ObjTrafo.tipoElemento.collarines))
                         {
                             if (pernoActivo)
                             {
                                 pernoActivo = false;
                             }
                             else
                             {
                                 pernoActivo = true;
                             }

                         }
                         else
                         {
                             BoxMessageManager._instance.MtdCreateBoxMessageInfo("El Objeto Seleccionado aun no se puede ubicar.", "ACEPTAR");

                         }
                         break;*/
            }
        }
        else
        {
            BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("textObjetoSeleccionado"), "ACEPTAR");
        }

    }

    public void BotonOprimido(int idButton)
    {
        idBotonActual = idButton;
        BotonesDelMenu[idButton].transform.GetChild(0).GetComponent<Image>().enabled = true;
    }

    private void salir()
    {
        panelPn.Mostrar(false);

    }

    private void activarPiezaTrafo(int id)
    {
        BotonMenuDeTransformador.interactable = false;
        ObjetosDelTrasformador[id].ActiveParteTrafo(true);
        objTrafoActual = ObjetosDelTrasformador[id];
        Mostrar(false);
    }

    private void desactivarSeleccion()
    {
        for (int i=0;i < BotonesDelMenu.Length; i++)
        {
            BotonesDelMenu[i].transform.GetChild(0).GetComponent<Image>().enabled = false;
        }
       
    }

    private void visualizarCables(bool SioNo)
    {
        for (int i = 0; i < cables.Length; i++)
            cables[i].SetActive(SioNo);
    }


}

[Serializable]
public class ObjTrafo
{
    #region members

    [SerializeField]
    private int NumEnlaSecuencia;

    [SerializeField]
    private GameObject[] ObjetoAActivar;

    private int NumerodeOBjetosAcomoados=0;

    public tipoElemento NombreDelElemento;

    public enum tipoElemento
    {
        collarines,
        diagonales,
        cruceta,
        transformador,
        dps,
        cintaMetalica,
        aisaldores,
        aisaldoresCarrete,
        cortacircuitos,
        SoporteAisladorCarrete,
        Perno

    }
    #endregion

    #region accesores

    public int _NumEnlaSecuencia
    {
        get
        {
            return NumEnlaSecuencia;
        }
    }
    public int _NumerodeOBjetosAcomoados
    {
        set
        {
            NumerodeOBjetosAcomoados = value;
        }

        get
        {
            return NumerodeOBjetosAcomoados;
        }
    }

    public GameObject[] _ObjetoAActivar
    {
        get
        {
            return ObjetoAActivar;
        }
    }

    #endregion

    #region public methods

    public void ActiveParteTrafo(bool argActivate)
    {
        foreach (var tmpZone in ObjetoAActivar)
            tmpZone.SetActive(argActivate);
    }

    public bool TodosLosOjetoPuesto()
    {
        NumerodeOBjetosAcomoados++;
        if (ObjetoAActivar.Length <= NumerodeOBjetosAcomoados)
            return true;
        else
            return false;
    }


    #endregion
}

