﻿using UnityEngine;

namespace NSSituacion2
{
    public class ControladorPerdigaConPinzas3 : MonoBehaviour
    {
        #region members

        [SerializeField] private GameObject[] pinzas;

        [SerializeField] private GameObject[] pinzasEnCables;

        [SerializeField] private GameObject contenedorZonasActivasPonerPinzas;

        [SerializeField] private GameObject[] zonasActivasPinzas;

        [SerializeField] private GameObject contenedorZonasActivasQuitarPinzas;

        [SerializeField] private GameObject[] zonasActivasQuitarPinzas;

        [SerializeField] private GameObject[] arrayPivotesCuerda;

        [SerializeField] private Vector3[] arrayPosicionesPivoteCuerdas;

        private int indexPinzaActual;

        private int[] ordenPinzasColocadas = new int[4];

        private int indexPinzaColocada;


        [Header("Check puesta a tierra colocada")]
        /// <summary>
        /// para permitir quitar el electrodo de la puesta a tierra
        /// </summary>
        [SerializeField]
        private QuitarElectrodoPuestaTierra refQuitarElectrodoPuestaTierra;


        public bool PuestaTierraColocada
        {
            get { return indexPinzaColocada > 0; }
        }

        #endregion

        public void SetPinza(int argIndexPinza)
        {
            ordenPinzasColocadas[indexPinzaColocada] = argIndexPinza;

            var tmpBaseOrden = 0;

            for (int i = 0; i < (argIndexPinza + 1); i++)
            {
                Debug.Log("!(ordenPinzasColocadas[i] == tmpBaseOrden)");
                if (!(ordenPinzasColocadas[i] == tmpBaseOrden))
                {
                    ReiniciarPertiga();
                    return;
                }

                tmpBaseOrden++;
            }

            pinzas[argIndexPinza].SetActive(false);
            pinzasEnCables[argIndexPinza].SetActive(true);
            zonasActivasPinzas[argIndexPinza].SetActive(false);

            arrayPivotesCuerda[argIndexPinza].transform.SetParent(pinzasEnCables[argIndexPinza].transform);
            arrayPivotesCuerda[argIndexPinza].transform.localPosition = arrayPosicionesPivoteCuerdas[argIndexPinza];

            indexPinzaColocada++;

            if (indexPinzaColocada == 4)
            {
                gameObject.SetActive(false);

                foreach (var tmpZonaActiva in zonasActivasQuitarPinzas)
                    tmpZonaActiva.SetActive(true);

                contenedorZonasActivasQuitarPinzas.SetActive(false);
            }
        }

        public void RemovePinza(int argIndexPinza)
        {
            indexPinzaColocada--;

            if (argIndexPinza == indexPinzaColocada)
            {
                pinzas[argIndexPinza].SetActive(true);
                pinzasEnCables[argIndexPinza].SetActive(false);
                zonasActivasQuitarPinzas[argIndexPinza].SetActive(false);

                arrayPivotesCuerda[argIndexPinza].transform.SetParent(transform);
                arrayPivotesCuerda[argIndexPinza].transform.localPosition = new Vector3(Random.Range(-1f, 1f), -20, 0);

                if (indexPinzaColocada == 0)
                {
                    gameObject.SetActive(false);
                    refQuitarElectrodoPuestaTierra._sePuedeQuitarElectrodo = true;

                    foreach (var tmpZonaActiva in zonasActivasPinzas)
                        tmpZonaActiva.SetActive(true);

                    contenedorZonasActivasPonerPinzas.SetActive(false);
                }
            }
            else
            {
                zonasActivasQuitarPinzas[argIndexPinza].SetActive(true);
                indexPinzaColocada++;
            }
        }

        public void ReiniciarPertiga()
        {
            indexPinzaColocada = 0;

            for (int i = 0; i < arrayPivotesCuerda.Length; i++)
            {
                pinzas[i].SetActive(true);
                pinzasEnCables[i].SetActive(false);
                zonasActivasPinzas[i].SetActive(true);
                zonasActivasQuitarPinzas[i].SetActive(true);
                arrayPivotesCuerda[i].transform.SetParent(transform);
                arrayPivotesCuerda[i].transform.localPosition = new Vector3(0, -20, 0);
            }
        }
    }
}