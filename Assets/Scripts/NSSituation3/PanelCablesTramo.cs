﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace NSSituacion3
{
    public class PanelCablesTramo : MonoBehaviour
    {
        [SerializeField] private Toggle[] arrayTogglesCalibreTramo;

        public void ActivarToggle(int argIndexCalibreSeleccionado)
        {
            arrayTogglesCalibreTramo[argIndexCalibreSeleccionado].isOn = true;
        }        
    }
}