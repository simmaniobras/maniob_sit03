﻿using NSBoxMessage;
using NSCreacionPDF;
using NSEvaluacion;
using NSInterfazAvanzada;
using NSSituacion1;
using NSSituacion2;
using NSTraduccionIdiomas;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NSInterfaz
{
    public class clsRegistroDatosSituacion2 : AbstractSingletonPanelUIAnimation<PanelInterfazRegistroDatos>
    {

        #region members

        [SerializeField] private ControladorDatosSesion refControladorDatosSesion;

        [SerializeField] private PanelInterfazEvaluacion refPanelInterfazEvaluacion;

        [SerializeField] private ControladorValoresPDF refControladorValoresPDF;

        [SerializeField] private Button buttonReporte;

        [SerializeField] private Evaluacion refEvaluacion;

        [SerializeField, Header("Corriente")]
        private TMP_InputField[] inputCorrienteAntesDelCambio;
        [SerializeField]
        private TMP_InputField[] inputCorrrienteDespuesDelCambio;

        [SerializeField, Header("Tension")]
        private TMP_InputField[] inputTensionAntesDelCambio;
        
        [SerializeField]
        private TMP_InputField[] inputTensionDespuesDelCambio;

        [SerializeField, Header("Prueba de potencia timepo")]
        private TMP_InputField[] inputPruebaPotencia;

        private float valorPorCampos = 1f / 17f;

        /// <summary>
        /// se llama a la funsion que retorne este vercoto en la situacion
        /// </summary>
        private float[] DatosTensionAntes = new float[3];

        /// <summary>
        /// se llama a la funsion que retorne este vercoto en la situacion
        /// </summary>
        private float[] DatosTensionDespues = new float[3];


        /// <summary>
        /// se llama a la funsion que retorne este vercoto en al situacion
        /// </summary>
        private float[] DatosCorrienteAntes = new float[3]; 

        /// <summary>
        /// se llama a la funsion que retorne este vercoto en al situacion
        /// </summary>
        private float[] DatosCorrienteDespues = new float[3];


        private float[] DatosinputPruebaPotencia = new float[5];

        //calificacion
        private float calificacion = 0;

        #endregion

        public void OnButtonValidar()
        {
            if (ValidarEmptyInputs())
            {
                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeCamposNecesarios"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                return;
            }

            if (ValidarDatos())
                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeValidarFelicitaciones"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
            else
            {
                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeValidarIncorrectos"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                refControladorDatosSesion.AddIntentos();
            }
        }

        public void OnButtonReporte()
        {
            if (ValidarDatos())
                BoxMessageManager._instance.MtdCreateBoxMessageDecision(DiccionarioIdiomas._instance.Traducir("mensajeValidarCorrectos"), DiccionarioIdiomas._instance.Traducir("TextCancelar"), DiccionarioIdiomas._instance.Traducir("TextReporte"), OnDatosCorrectos);
            else
            {
                BoxMessageManager._instance.MtdCreateBoxMessageDecision(DiccionarioIdiomas._instance.Traducir("mensajeValidarIncorrectosReporte"), DiccionarioIdiomas._instance.Traducir("TextCancelar"), DiccionarioIdiomas._instance.Traducir("TextReporte"), OnDatosCorrectos);
                refControladorDatosSesion.AddIntentos();
            }
        }

        public void SetDatosTensionAntes(float argTensionX1, float argTensionX2, float argTensionX3)
        {
            DatosTensionAntes[0] = argTensionX1;
            DatosTensionAntes[1] = argTensionX2;
            DatosTensionAntes[2] = argTensionX3;
        }

        public void SetDatosTensionDespues(float argTensionX1, float argTensionX2, float argTensionX3)
        {
            DatosTensionDespues[0] = argTensionX1;
            DatosTensionDespues[1] = argTensionX2;
            DatosTensionDespues[2] = argTensionX3;
        }

        public void SetDatosCorrienteAntes(float argCorrienteX1, float argCorrienteX2, float argCorrienteX3)
        {
            DatosCorrienteAntes[0] = argCorrienteX1;
            DatosCorrienteAntes[1] = argCorrienteX2;
            DatosCorrienteAntes[2] = argCorrienteX3;
        }

        public void SetDatosCorrienteDespues(float argCorrienteX1, float argCorrienteX2, float argCorrienteX3)
        {
            DatosCorrienteDespues[0] = argCorrienteX1;
            DatosCorrienteDespues[1] = argCorrienteX2;
            DatosCorrienteDespues[2] = argCorrienteX3;
        }

        public void SetDatosinputPruebaPotencia(float argPotenciaRed, float argPotenciaInstalada, float argConstanteMedidor, float argErrorMedidor, float argTiempo)
        {
            DatosinputPruebaPotencia[0] = argPotenciaRed;
            DatosinputPruebaPotencia[1] = argPotenciaInstalada;
            DatosinputPruebaPotencia[2] = argConstanteMedidor;
            DatosinputPruebaPotencia[3] = argErrorMedidor;
            DatosinputPruebaPotencia[4] = argTiempo;
        }


        private void OnDatosCorrectos()
        {
            Mostrar(false);
            refControladorValoresPDF.SetPanelRegistroDatos();
            refPanelInterfazEvaluacion.Mostrar();
            //buttonReporte.interactable = true;
        }

        private bool ValidarDatos()
        {
            calificacion = 0;
            //tension

            for (int i = 0; i < 3; i++)
            {
                /// correinte antes del cambio del dps
                EvaluarYCompararCampo(inputCorrienteAntesDelCambio[i], DatosCorrienteAntes[i]);

                /// Tension antes del cambio del dps
                EvaluarYCompararCampo(inputTensionAntesDelCambio[i], DatosTensionAntes[i]);
                
                /// correinte Despues del cambio del dps
                EvaluarYCompararCampo(inputCorrrienteDespuesDelCambio[i], DatosCorrienteDespues[i]);
                
                /// Tension Despues del cambio del dps
                EvaluarYCompararCampo(inputTensionDespuesDelCambio[i], DatosTensionDespues[i]);
            }

            for (int i = 0; i < DatosinputPruebaPotencia.Length; i++)
            {
                EvaluarYCompararCampo(inputPruebaPotencia[i], DatosinputPruebaPotencia[i]);
            }

            Debug.Log("este es la calificacion que en el nuevo registro de datos= " + calificacion);
            refEvaluacion.AsignarCalificacionRegistroDatos(calificacion);
            
            if (Mathf.Approximately(calificacion, 1f))
                return true;
            
            return false;
        }

        private void EvaluarYCompararCampo(TMP_InputField CampoAEvaluar, float DatoParaComparar)
        {
            float tmpinputCorrienteAntesDelCambio;
            
            /// correinte antes del cambio del dps
            var tmpTextoCampoAEvaluar = CampoAEvaluar.text;

            if (tmpTextoCampoAEvaluar.Length > 0)
            {
                float.TryParse(tmpTextoCampoAEvaluar, out tmpinputCorrienteAntesDelCambio);
            
                var errorCorrienteAntes = Mathf.Abs(tmpinputCorrienteAntesDelCambio - DatoParaComparar) <= 0.05;  //Mathf.Abs(1 - (tmpinputCorrienteAntesDelCambio * 1 / DatoParaComparar)) <= 0.05;
                print("volor dado por el usuario= "+ tmpinputCorrienteAntesDelCambio+"valor del simulador= "+DatoParaComparar);
            
                if (errorCorrienteAntes)
                {
                    calificacion = valorPorCampos + calificacion;
                    CampoAEvaluar.transform.GetChild(0).gameObject.SetActive(false);
                }
                else
                    CampoAEvaluar.transform.GetChild(0).gameObject.SetActive(true);
            }
            else
            {
                CampoAEvaluar.transform.GetChild(0).gameObject.SetActive(true);
                Debug.Log("Input registro datos sin texto :" + tmpTextoCampoAEvaluar);
            }
        }
        
        private bool ValidarEmptyInputs()
        {
            var tmpEmptyInputs = 0;

            for (int i = 0; i < 3; i++)
            {
                if (inputCorrienteAntesDelCambio[i].text.Equals(""))
                    tmpEmptyInputs++;

                if (inputCorrrienteDespuesDelCambio[i].text.Equals(""))
                    tmpEmptyInputs++;

                if (inputTensionAntesDelCambio[i].text.Equals(""))
                    tmpEmptyInputs++;

                if (inputCorrrienteDespuesDelCambio[i].text.Equals(""))
                    tmpEmptyInputs++;
            }

            for (int i = 0; i < 4; i++)
            {
                if (inputPruebaPotencia[i].text.Equals(""))
                    tmpEmptyInputs++;
            }

            return tmpEmptyInputs > 0;

        }

        private void ResetInputsIncorrectos()
        {
            for (int i = 0; i < 3; i++)
            {
                inputCorrienteAntesDelCambio[i].transform.GetChild(0).gameObject.SetActive(false);
                inputCorrienteAntesDelCambio[i].text = "";

                inputCorrrienteDespuesDelCambio[i].transform.GetChild(0).gameObject.SetActive(false);
                inputCorrrienteDespuesDelCambio[i].text = "";

                inputTensionAntesDelCambio[i].transform.GetChild(0).gameObject.SetActive(false);
                inputTensionAntesDelCambio[i].text = "";

                inputCorrrienteDespuesDelCambio[i].transform.GetChild(0).gameObject.SetActive(false);
                inputCorrrienteDespuesDelCambio[i].text = "";

            }

            for (int i = 0; i < 4; i++)
            {
                inputPruebaPotencia[i].transform.GetChild(0).gameObject.SetActive(false);
                inputPruebaPotencia[i].text = "";
            }
        }

        private void SetInputIncorrecto(TMP_InputField argInputField, bool argIncorrecto = true)
        {
            argInputField.transform.Find("ImageBadInput").gameObject.SetActive(argIncorrecto);
        }
    }
}