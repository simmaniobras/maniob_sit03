﻿using System;
using NSBoxMessage;
using NSInterfaz;
using NSTraduccionIdiomas;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace NSSituacion5
{
    public class ControladorConexionesMacroMedidor : MonoBehaviour
    {
        [SerializeField] private Color[] arrayColoresCable;

        [SerializeField] private CableV[] arrayCablesConexionV;

        [SerializeField] private CableTC[] arrayCablesConexionTC;

        [SerializeField] private CablePuente[] arrayCablesConexionPuenteDistancia1;

        [SerializeField] private CablePuente[] arrayCablesConexionPuenteDistancia2;

        [SerializeField] private CablePuente[] arrayCablesConexionPuenteDistancia3;

        [SerializeField] private GameObject[] arrayButtonsBornes;

        [SerializeField] private GameObject[] arrayButtonsBornesPuente;

        [SerializeField] private GameObject buttonCableV;

        [SerializeField] private GameObject buttonCableTc;

        [SerializeField] private VentanaSeleccionCable refVentanaSeleccionCableTc;
        
        [SerializeField] private VentanaSeleccionCable refVentanaSeleccionCableV;

        [SerializeField] private PanelInterfazSoloAnimacion refPanelInterfazSeleccionCablesTc;
        
        [SerializeField] private PanelInterfazSoloAnimacion refPanelInterfazSeleccionCablesV;

        [SerializeField] private ControladorSituacion5 refControladorSituacion5;

        private Color colorPuente;

        private int indexBorneSeleccionado;

        /// <summary>
        /// Conexiones de 3 cables, que representan una conexion completa, ejemplo = amarillo TC S1, amarillo V, amarillo TC S2
        /// </summary>
        private Conexion3Cables[] arrayConexion3Cables = new Conexion3Cables[3];//para las conexiones de los 3 ciclos de cables, 

        public Conexion3Cables[] ArrayConexion3Cables
        {
            get { return arrayConexion3Cables; }
        }

        /// <summary>
        /// Conexion del cable blanco
        /// </summary>
        private ConexionCable conexionCableBlanco;
        
        public UnityEvent OnCableConectado;
        
        public bool _macromedidorConectado
        {
            get
            {
                if (arrayConexion3Cables[0] == null)//si no se han inicalizado los inicializo
                {
                    arrayConexion3Cables[0] = new Conexion3Cables();
                    arrayConexion3Cables[1] = new Conexion3Cables();
                    arrayConexion3Cables[2] = new Conexion3Cables();
                }
                
                var tmpConexionNormal = arrayConexion3Cables[0].VerificarConexionNormal() && arrayConexion3Cables[1].VerificarConexionNormal() && arrayConexion3Cables[2].VerificarConexionNormal() && (conexionCableBlanco != null? conexionCableBlanco.IndexColorCable == 6: false);
                var tmpConexionPuente = arrayConexion3Cables[0].VerificarConexionPuente(3) && arrayConexion3Cables[1].VerificarConexionPuente(3) && arrayConexion3Cables[2].VerificarConexionPuente(1) && (conexionCableBlanco != null? conexionCableBlanco.IndexColorCable == 6: false);

                return tmpConexionNormal || tmpConexionPuente;
            }
        }

        private void Awake()
        {
            refVentanaSeleccionCableTc.OnFinishConexionCable.AddListener(OnCableTcCambiado);
            //refVentanaSeleccionCableTc.OnFinishConexionCable.AddListener((arg0, toggle, i) =>   ConfirmarCorrectaIncorrectaConexion());
            refVentanaSeleccionCableV.OnFinishConexionCable.AddListener(OnCableVCambiado);
            //refVentanaSeleccionCableV.OnFinishConexionCable.AddListener((arg0, toggle, i) =>   ConfirmarCorrectaIncorrectaConexion());
            OnCableConectado.AddListener(ConfirmarCorrectaIncorrectaConexion);
        }

        public void SeleccionarBorne(int argIndexBorneSeleccionado)
        {
            /*if (!refControladorSituacion5.CortaCircuitosAbiertos)
            {
                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeMacromedidorNoSePuedeModificar") , DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                return;
            }*/
            
            indexBorneSeleccionado = argIndexBorneSeleccionado;
            buttonCableV.SetActive(true);
            buttonCableTc.SetActive(true);

            foreach (var tmpBorne in arrayButtonsBornes)
                tmpBorne.SetActive(false);

            if (argIndexBorneSeleccionado < 9)//si se selecciona cualquier borne excepto el ultimo
            {
                for (int i = argIndexBorneSeleccionado + 1; i < Mathf.Clamp(argIndexBorneSeleccionado + 4, 0, arrayButtonsBornesPuente.Length); i++)
                    arrayButtonsBornesPuente[i].SetActive(true);

                var tmpIndexConexion3Cables = GetIndexBorneConexion3Cables(argIndexBorneSeleccionado);
                Debug.Log(arrayConexion3Cables[tmpIndexConexion3Cables]);
                colorPuente = arrayConexion3Cables[tmpIndexConexion3Cables].GetColorPuente(indexBorneSeleccionado);
            }
        }

        public void SeleccionarCableV()
        {
            refPanelInterfazSeleccionCablesV.Mostrar();
        }

        public void SeleccionarCableTc()
        {
            refPanelInterfazSeleccionCablesTc.Mostrar();
        }

        public void DesconectarCableDeBorne(int argIndexBorne)
        {
            if (argIndexBorne < 9)
            {
                var tmpIndexConexion3Cables = GetIndexBorneConexion3Cables(argIndexBorne);
                arrayConexion3Cables[tmpIndexConexion3Cables].DesconectarCable(argIndexBorne);
            }
            else
            {
                conexionCableBlanco.DesconectarCable();
                conexionCableBlanco = null;
            }
        }
        
        public void SeleccionarPuenteBorne(int argIndexBorneSeleccionado)
        {
            var tmpDiferenciaBorneInicialFinal = argIndexBorneSeleccionado - indexBorneSeleccionado;
            var tmpIndexConexion3Cables = GetIndexBorneConexion3Cables(indexBorneSeleccionado);
            
            switch (tmpDiferenciaBorneInicialFinal)
            {
                case 1:
                    
                    arrayConexion3Cables[tmpIndexConexion3Cables].ConectarCable(indexBorneSeleccionado, 
                        new ConexionCable(arrayCablesConexionPuenteDistancia1[indexBorneSeleccionado],
                            colorPuente,
                            indexBorneSeleccionado,
                            null,
                            2,
                            0,
                            -1,
                            indexBorneSeleccionado,
                            argIndexBorneSeleccionado));
                    break;

                case 2:
                    
                    arrayConexion3Cables[tmpIndexConexion3Cables].ConectarCable(indexBorneSeleccionado, 
                        new ConexionCable(arrayCablesConexionPuenteDistancia2[indexBorneSeleccionado],
                            colorPuente,
                            indexBorneSeleccionado,
                            null,
                            2,
                            0,
                            -1,
                            indexBorneSeleccionado,
                            argIndexBorneSeleccionado));
                    break;

                case 3:
                    
                    arrayConexion3Cables[tmpIndexConexion3Cables].ConectarCable(indexBorneSeleccionado, 
                        new ConexionCable(arrayCablesConexionPuenteDistancia3[indexBorneSeleccionado],
                            colorPuente,
                            indexBorneSeleccionado,
                            null,
                            2,
                            0,
                            -1,
                            indexBorneSeleccionado,
                            argIndexBorneSeleccionado));
                    break;
            }

            OnCableConectado.Invoke();
            buttonCableV.SetActive(false);
            buttonCableTc.SetActive(false);

            foreach (var tmpBorne in arrayButtonsBornes)
                tmpBorne.SetActive(true);

            foreach (var tmpBorne in arrayButtonsBornesPuente)
                tmpBorne.SetActive(false);
        }

        public int GetSentidoCorrienteConexion3Cables(int argIndexConexion)
        {
            if (ArrayConexion3Cables[argIndexConexion] != null)
                if (ArrayConexion3Cables[argIndexConexion].ArrayConexionCable[0] != null)
                    return ArrayConexion3Cables[argIndexConexion].ArrayConexionCable[0].GetSentidoCorrienteCaras();
            
            return 1;
        }
        
        /// <summary>
        /// Cuando se conectan todos los cables este metodo confirma si se conectaron bien o mal
        /// </summary>
        private void ConfirmarCorrectaIncorrectaConexion()
        {
            var tmpCantidadCablesConectados = arrayConexion3Cables[0].GetCantidadCablesConectados() + arrayConexion3Cables[1].GetCantidadCablesConectados() + arrayConexion3Cables[2].GetCantidadCablesConectados() + (conexionCableBlanco != null ? conexionCableBlanco.IndexColorCable == 6 ? 1 : 0 : 0);

            if (tmpCantidadCablesConectados == 10)
            {
                if (_macromedidorConectado)
                    BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeMacromedidorConexionCorrecta"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                else
                    BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeMacromedidorConexionIncorrecta"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
            }
        }

        /// <summary>
        /// Se ejecuta cuando en la ventana de seleccion del cable TC se preciona aceptar
        /// </summary>
        /// <param name="argIndexCableSeleccionado">Color del cable seleccionado</param>
        /// <param name="argIndexCaraS">Indice de la cara S seleccionada, S1 = 1, S2 = 2</param>
        private void OnCableTcCambiado(int argIndexCableSeleccionado, Toggle argToggleCableWindow, int argIndexCaraS)
        {
            if (indexBorneSeleccionado < 9)
            {
                var tmpIndexConexion3Cables = GetIndexBorneConexion3Cables(indexBorneSeleccionado);
            
                arrayConexion3Cables[tmpIndexConexion3Cables].ConectarCable(indexBorneSeleccionado, 
                    new ConexionCable(arrayCablesConexionTC[indexBorneSeleccionado],
                        arrayColoresCable[argIndexCableSeleccionado],
                        argIndexCableSeleccionado,
                        argToggleCableWindow,
                        (argIndexCaraS % 2) + 1,// este en realidad viene con el indice del toggle seleccioando, es decir, 0, 1, 2, 3, 4 ,5  
                        refControladorSituacion5.GetCaraIndexColorCableConectado( Mathf.FloorToInt(argIndexCableSeleccionado / 2f)) ? 2: 1, 
                        0,
                        -1, 
                        indexBorneSeleccionado
                    ));
            }
            else
            {
                if (conexionCableBlanco != null)
                    conexionCableBlanco.DesconectarCable();
                
                Debug.Log(argIndexCableSeleccionado);
                conexionCableBlanco = new ConexionCable(arrayCablesConexionTC[9], 
                    arrayColoresCable[argIndexCableSeleccionado], 
                    argIndexCableSeleccionado, 
                    argToggleCableWindow,
                    0,
                    0,
                    1,
                    -1,
                    indexBorneSeleccionado
                );
            }
            
            OnCableConectado.Invoke();
            buttonCableV.SetActive(false);
            buttonCableTc.SetActive(false);
            
            foreach (var tmpBorne in arrayButtonsBornes)
                tmpBorne.SetActive(true);

            foreach (var tmpBorne in arrayButtonsBornesPuente)
                tmpBorne.SetActive(false);
        }

        /// <summary>
        /// Se ejecuta cuando en la ventana de seleccion del cable V se preciona aceptar
        /// </summary>
        /// <param name="argIndexCableSeleccionado">Color del cable seleccionado </param>
        /// <param name="argIndexCaraS">Indice que para el V siempre es 1</param>
        private void OnCableVCambiado(int argIndexCableSeleccionado, Toggle argToggleCableWindow, int argIndexCaraS = 1)
        {
            if (indexBorneSeleccionado < 9)
            {
                var tmpIndexConexion3Cables = GetIndexBorneConexion3Cables(indexBorneSeleccionado);

                arrayConexion3Cables[tmpIndexConexion3Cables].ConectarCable(indexBorneSeleccionado, 
                    new ConexionCable(arrayCablesConexionV[indexBorneSeleccionado],
                        arrayColoresCable[argIndexCableSeleccionado],
                        argIndexCableSeleccionado,
                        argToggleCableWindow,
                        argIndexCaraS,
                        -1,
                        1,
                        -1,
                        indexBorneSeleccionado
                    ));
            }
            else
            {
                if (conexionCableBlanco != null)
                    conexionCableBlanco.DesconectarCable();
                
                conexionCableBlanco = new ConexionCable(arrayCablesConexionV[9], 
                    Color.white, 
                    argIndexCableSeleccionado, 
                    argToggleCableWindow,
                    0,   
                    -1,   
                     1,
                    indexBorneSeleccionado,
                    indexBorneSeleccionado
                );
            }

            OnCableConectado.Invoke();
            
            buttonCableV.SetActive(false);
            buttonCableTc.SetActive(false);

            foreach (var tmpBorne in arrayButtonsBornes)
                tmpBorne.SetActive(true);

            foreach (var tmpBorne in arrayButtonsBornesPuente)
                tmpBorne.SetActive(false);
        }
        
        /// <summary>
        /// Conseguir el indice representativo para una de las conexiones de 3 cables
        /// </summary>
        /// <param name="argIndexBorne0_9"></param>
        /// <returns></returns>
        private int GetIndexBorneConexion3Cables(int argIndexBorne0_9)
        {
            return Mathf.FloorToInt(argIndexBorne0_9/3f);
        } 
    }

    
    public class ConexionCable
    {
        public AbstractCable Cable { get; private set; }
        
        /// <summary>
        /// Color del cable Conectado, amarillo claro, azul claro, rojo claro, blanco claro, amarillo oscuro, azul oscuro, rojo oscuro
        /// </summary>
        public Color ColorCable { get; private set; }

        /// <summary>
        /// Indice del color del cable, 0 = amarillo claro, 1 = amarillo oscuro, 2 = azul claro, 3 = azul oscuro, 4 = rojo claro, 5 = rojo oscuro, 6 = blanco
        /// </summary>
        public int IndexColorCable { get; private set;}
        
        /// <summary>
        /// Toggle de seleccion en la ventana
        /// </summary>
        public Toggle ToggleCableInWindow { get; private set; }
        
        /// <summary>
        /// Cara 1 = S1 o 2 = S2
        /// </summary>
        public int CaraS { get; private set; } 
        
        /// <summary>
        /// Cara 1 = S1 o 2 = S2 que se asigno en el anillo transformador
        /// </summary>
        public int CaraSAnilloTransformador { get; private set; } 

        /// <summary>
        /// Index del cable por el que entra la corriente,-1 =Puente, 0 = TC, V = 1
        /// </summary>
        public int IndexCableEntrada { get; private set; }

        /// <summary>
        /// Index del borne por donde inicia la entrada, esto es para los puentes 
        /// </summary>
        public int IndexBorneEntrada { get; private set;}
        
        /// <summary>
        /// Index del borne por donde sale la señal
        /// </summary>
        public int IndexBorneSalida { get; private set;}
        
        /// <summary>
        /// Crea una nueva conexion de cable con las siguientes propiedades
        /// </summary>
        /// <param name="argCable">Clase controladora del cable</param>
        /// <param name="argColorCable">Color real del cable conectado</param>
        /// <param name="argIndexColorCable">Indice del color del cable, 0 = amarillo claro, 1 = amarillo oscuro, 2 = azul claro, 3 = azul oscuro, 4 = rojo claro, 5 = rojo oscuro, 6 = blanco</param>
        /// <param name="argToggleCableWindow">Toggle de seleccion en la ventana</param>
        /// <param name="argCaraS">Cara 1 = S1 o 2 = S2</param>
        /// <param name="argIndexCableEntrada">Index del cable por el que entra la corriente,-1 = puente, 0 = TC, V = 1</param>
        /// <param name="argIndexBorneEntrada">Index del borne por donde inicia la entrada, esto es para los puentes </param>
        /// <param name="argIndexBorneSalida">Index del borne por donde sale la señal desde el TC o V hasta el macromedidor</param>
        public ConexionCable(AbstractCable argCable, Color argColorCable, int argIndexColorCable, Toggle argToggleCableWindow, int argCaraS, int argCaraSAnilloTransformador, int argIndexCableEntrada, int argIndexBorneEntrada, int argIndexBorneSalida)
        {
            Cable = argCable;
            ColorCable = argColorCable;
            Cable.ColorCable = ColorCable;//asigno el color del cable
            Cable.ActivarCable();
            
            IndexColorCable = argIndexColorCable;
            ToggleCableInWindow = argToggleCableWindow;
            
            if (ToggleCableInWindow)
                ToggleCableInWindow.interactable = false;
            
            CaraS = argCaraS;
            CaraSAnilloTransformador = argCaraSAnilloTransformador;
            IndexCableEntrada = argIndexCableEntrada;
            IndexBorneEntrada = argIndexBorneEntrada;
            IndexBorneSalida = argIndexBorneSalida;
            Debug.Log(ToString());
        }

        public void DesconectarCable()
        {
            if (ToggleCableInWindow)
                ToggleCableInWindow.interactable = true;
            else
                Debug.Log("No hay cable para desabilitar");
            
            Cable.DesactivarCable();
        }

        /// <summary>
        /// Consigue el sentido de la corriente, si es -1 o +1, segun las caras 
        /// </summary>
        /// <returns>retorna -1 si las caras S son diferentes entre la seleccionada en la ventana y la cara en el anillo</returns>
        public int GetSentidoCorrienteCaras()
        {
            return CaraS == CaraSAnilloTransformador ? 1 : -1;
        }
        
        public override string ToString()
        {
            return " //Nueva conexion// Cable.name : " + Cable.name + " IndexColorCable : " + IndexColorCable + " CaraS : " + CaraS + " CaraSAnilloTransformador : " + CaraSAnilloTransformador +" IndexCableEntrada : " + IndexCableEntrada + " IndexBorneEntrada : " + IndexBorneEntrada + " IndexBorneSalida : " + IndexBorneSalida;
        }
    }

    /// <summary>
    /// Representa la conexion de 3 cables
    /// </summary>
    public class Conexion3Cables
    {
        private ConexionCable[] arrayConexionCable = new ConexionCable[3];
        private ConexionCable[] arrayConexionCablePuente = new ConexionCable[3];
        
        public ConexionCable[] ArrayConexionCable
        {
            set { arrayConexionCable = value;}
            get { return arrayConexionCable; } 
        }
        
        public ConexionCable[] ArrayConexionCablePuente
        {
            set { arrayConexionCablePuente = value;}
            get { return arrayConexionCablePuente; } 
        }
        
        public Conexion3Cables()
        {
            ArrayConexionCable = new ConexionCable[3];
            ArrayConexionCablePuente = new ConexionCable[3];
        }
        
        /// <summary>
        /// Conectar un cable
        /// </summary>
        /// <param name="argIndexBorne0_9">Indice del borne del que se quiere conectar, desde 0 hasta 2</param>
        /// <param name="argConexionCable">Conexion del cable</param>
        public void ConectarCable(int argIndexBorne0_9, ConexionCable argConexionCable)
        {
            var tmpIndexBorneInterno = GetIndexBorneInterno(argIndexBorne0_9);
            var tmpConexionCable = ArrayConexionCable[tmpIndexBorneInterno];//busco una conexion de cable anterior
            var tmpConexionCablePuente = ArrayConexionCablePuente[tmpIndexBorneInterno];

            if (tmpConexionCable != null) //si al conexion de cable es diferente de null e porque ya existia una conexion
            {
                if (argConexionCable.IndexCableEntrada == -1)//si la conexion que voy a hacer es un puente
                {
                    if (tmpConexionCablePuente != null)//miro si existia un puente anteriormente
                        tmpConexionCablePuente.DesconectarCable();//lo desconecto

                    ArrayConexionCablePuente[tmpIndexBorneInterno] = argConexionCable;
                    ArrayConexionCablePuente[tmpIndexBorneInterno].Cable.ActivarCable();
                    Debug.Log(" tmpIndexBorneInterno : "+tmpIndexBorneInterno);
                }
                else
                {
                    tmpConexionCable.DesconectarCable();//entonces desconecto el cable
                    ArrayConexionCable[tmpIndexBorneInterno] = argConexionCable;//asigno la conexion nueva
                    ArrayConexionCable[tmpIndexBorneInterno].Cable.ActivarCable();
                }
            }
            else
            {
                if (argConexionCable.IndexCableEntrada == -1) //si la conexion que voy a hacer es un puente
                {
                    ArrayConexionCablePuente[tmpIndexBorneInterno] = argConexionCable;
                    ArrayConexionCablePuente[tmpIndexBorneInterno].Cable.ActivarCable();
                }
                else
                {
                    ArrayConexionCable[tmpIndexBorneInterno] = argConexionCable;//asigno la conexion nueva
                    ArrayConexionCable[tmpIndexBorneInterno].Cable.ActivarCable();
                }
            }
        }

        /// <summary>
        /// Desconectar un cable
        /// </summary>
        /// <param name="argIndexBorne0_9">Indice del borne del que se quiere desconectar, desde 0 hasta 2</param>
        public void DesconectarCable(int argIndexBorne0_9)
        {
            var tmpIndexBorneInterno = GetIndexBorneInterno(argIndexBorne0_9);
            
            var tmpConexionCablePuente = ArrayConexionCablePuente[tmpIndexBorneInterno];

            if (tmpConexionCablePuente != null)
            {
                tmpConexionCablePuente.DesconectarCable();
                ArrayConexionCablePuente[tmpIndexBorneInterno] = null;
                return;
            }
            
            var tmpConexionCable = ArrayConexionCable[tmpIndexBorneInterno];

            if (tmpConexionCable != null)
            {
                tmpConexionCable.DesconectarCable();
                ArrayConexionCable[tmpIndexBorneInterno] = null;
            }
        }
        
        /// <summary>
        /// verifica si la conexion de esta tripleta quedo bien hecha
        /// </summary>
        /// <returns>true si la conexion esta bien hecha</returns>
        public bool VerificarConexionNormal()
        {
            if (ArrayConexionCable[0] == null || ArrayConexionCable[1] == null || ArrayConexionCable[2] == null)
                return false;
            
            var tmpConexionesCorrectas = 0;
            
            if (ArrayConexionCable[0].IndexCableEntrada == 0 && ArrayConexionCable[0].IndexBorneSalida % 3 == 0)
                tmpConexionesCorrectas++;

            if (ArrayConexionCable[1].IndexCableEntrada == 1 && ArrayConexionCable[1].IndexBorneSalida % 3 == 1)
                tmpConexionesCorrectas++;
            
            if (ArrayConexionCable[2].IndexCableEntrada == 0 && ArrayConexionCable[2].IndexBorneSalida % 3 == 2)
                tmpConexionesCorrectas++;

            var tmpColorCable = ArrayConexionCable[0].IndexColorCable;
            
            if (tmpColorCable + 1 == ArrayConexionCable[1].IndexColorCable)//Verifico el cable V 
                tmpConexionesCorrectas++;
            
            if (tmpColorCable == ArrayConexionCable[2].IndexColorCable)
                tmpConexionesCorrectas++;
            
            return tmpConexionesCorrectas == 5;
        }

        public bool VerificarConexionPuente(int argDistanciaPuente)
        {
            if (arrayConexionCable[0] == null || arrayConexionCable[1] == null || arrayConexionCablePuente[2] == null)
                return false;
            
            var tmpConexionesCorrectas = 0;
            
            if (arrayConexionCable[0].IndexCableEntrada == 0 && arrayConexionCable[0].IndexBorneSalida % 3 == 0)
                tmpConexionesCorrectas++;

            if (arrayConexionCable[1].IndexCableEntrada == 1 && arrayConexionCable[1].IndexBorneSalida % 3 == 1)
                tmpConexionesCorrectas++;
            
            if (arrayConexionCablePuente[2].IndexCableEntrada == -1 && (arrayConexionCablePuente[2].IndexBorneSalida - arrayConexionCablePuente[2].IndexBorneEntrada == argDistanciaPuente))
                tmpConexionesCorrectas++;
            
            Debug.Log("tmpConexionesCorrectasPuente : " + tmpConexionesCorrectas);
            
            var tmpColorCable = ArrayConexionCable[0].IndexColorCable;
            
            if (tmpColorCable + 1 == ArrayConexionCable[1].IndexColorCable)//Verifico el cable V 
                tmpConexionesCorrectas++;

            return tmpConexionesCorrectas == 4;
        }

        public int GetCantidadCablesConectados()
        {
            bool[] tmpCableContado = {false, false, false};//Para saber que cables se contaron ya
            
            var tmpCantidadCablesConexionNormal = 0;
            
            for (var i = 0; i < ArrayConexionCable.Length; i++)
                if (ArrayConexionCable[ i] != null)
                {
                    tmpCantidadCablesConexionNormal++;
                    tmpCableContado[i] = true;//asigno que este cable ya se conto
                }
            
            var tmpCantidadCablesConexionPuente = 0;
            
            for (var i = 0; i < ArrayConexionCablePuente.Length; i++)
                if (ArrayConexionCablePuente[i] != null && !tmpCableContado[i])
                    tmpCantidadCablesConexionPuente++;

            
            var tmpCantidadCablesConectados = tmpCantidadCablesConexionNormal + tmpCantidadCablesConexionPuente;
            Debug.Log("GetCantidadCablesConectados : " + tmpCantidadCablesConectados);
            
            return tmpCantidadCablesConectados;
        }
        
        /// <summary>
        /// Consigo el color del cable para hacer puente
        /// </summary>
        /// <param name="argIndexBorne0_9">Indice del borne desde donde comienza el puente</param>
        /// <returns>Color del cable para el puente</returns>
        public Color GetColorPuente(int argIndexBorne0_9)
        {
            if (ArrayConexionCablePuente != null)
            {
                Debug.Log("GetIndexBorneInterno(argIndexBorne0_9) : "+argIndexBorne0_9+"  "+ GetIndexBorneInterno(argIndexBorne0_9));
                
                var tmpConexionCablePuente = ArrayConexionCablePuente[GetIndexBorneInterno(argIndexBorne0_9)];//busco una conexion de cable anterior

                if (tmpConexionCablePuente != null)//si al conexion de cable es diferente de null e porque ya existia una conexion
                    return tmpConexionCablePuente.ColorCable;
            }

            return Color.white;
        }

        /// <summary>
        /// Convierte el indice que va desde 0 a 9, a un indice interno que vaya desde 0 a 2
        /// </summary>
        /// <param name="argIndexBorne0_9">Indice real de 0 a 9</param>
        /// <returns>Indice entre 0 y 2</returns>
        private int GetIndexBorneInterno(int argIndexBorne0_9)
        {
            return argIndexBorne0_9 % 3;
        }
    }
}