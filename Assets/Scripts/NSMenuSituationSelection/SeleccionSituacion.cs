using NSActiveZones;
using NSSeguridad;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using NSActiveZones;

namespace NSMenuSituationSelection
{
    public class SeleccionSituacion : MonoBehaviour
    {
        #region members
        private bool SeInicio;
      
        #endregion

        #region accesors

        #endregion

        #region mono behaviour

        private void Awake()
        {

        }

        public void Start()
        {
            SeInicio = false;
            SeleccionSituacion1();
        }
        #endregion

        #region public methods

        private IEnumerator cargarPractica()
        {
            yield return new WaitForEndOfFrame();
            SceneManager.LoadScene("Situation3", LoadSceneMode.Additive);
        }

        public void SeleccionSituacion1()
        {
            //SceneManager.LoadScene("Situation3",LoadSceneMode.Additive);
            StartCoroutine(cargarPractica());
        }

        public void activarBienvenida()
        {
            if (!SeInicio)
            {
                GameObject.FindGameObjectWithTag("bienvenidaControl").GetComponent<BienvenidaControl>().ActivarBienvinida();
                SeInicio = true;
            }
            else
            {
                StartCoroutine(Activarpractica());
            }
        }

        private IEnumerator Activarpractica()
        {
            SceneManager.UnloadSceneAsync("Situation3");
            SceneManager.LoadScene("Situation3", LoadSceneMode.Additive);
            yield return new WaitForSeconds(1);
            GameObject.FindGameObjectWithTag("activarZonas").GetComponent<ActiveZonesController>().ActiveAllZones();
            GameObject.FindGameObjectWithTag("activarZonas").GetComponent<activarInterfaz>().activarInterfazGeneral();

        }
        #endregion

    }
}