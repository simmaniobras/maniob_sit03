﻿using NSActiveZones;
using NSBoxMessage;
using NSCelular;
using NSInterfaz;
using System;
using NSTraduccionIdiomas;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace NSSituacion1
{
    public class ControladorSituacion1 : MonoBehaviour
    {
        #region members

        private bool zonaSeguridadColocada;

        private bool avatarVestido;

        [SerializeField] private Button buttonCelular;

        [SerializeField] private ControladorCelular refControladorCelular;

        [SerializeField] private ActiveZonesController refActiveZonesController;

        [SerializeField] private CortaCircuitoDesconectadoRamdom refCortaCircuitoDesconectadoRamdom;

        [SerializeField] private PanelInterfazRegistroDatos refPanelInterfazRegistroDatos;

        private byte mensajesEnviadosCentroControl;

        private EtapaSituacionActual etapaSituacionActual = EtapaSituacionActual.Null;

        [SerializeField, Header("Valores transformador")]
        private Transformador[] arrayTransformadores;

        [SerializeField] private float[] tipoFusible;

        [SerializeField] private Image imageTransformador;

        private int indexTransformadorSeleccionado;

        private float[] factorDivisionSegunTransformador = {20, 20, 30, 40, 80};
        
        private float[] corrienteCablesTransformador = new float[3];

        private float[] corrienteCablesMacromedidor = new float[3];

        private float[] tensionLineaPuntasPinza = new float[3];

        private float[] tensionFasePuntasPinza = new float[3];

        private float[] potenciaMacroMedidor = new float[3];

        private float corrientePantallaMacroMedidor;

        private float potenciaTransformadorActual;

        [SerializeField] private TextMeshProUGUI textPantallaMacroMedidor;
        #endregion

        #region Herramientas

        [SerializeField, Header("Herramientas"), Space(10)]
        private GameObject puntaDetectora;

        [SerializeField] private GameObject escalera;

        [SerializeField] private GameObject imageEscalera;

        [SerializeField] private GameObject escaleraCamioneta;

        [SerializeField] private GameObject pinzaAmperimetro;

        [SerializeField] private GameObject bastonPerdiga;

        /*[SerializeField]
        private GameObject letreroNoOperar;*/

        [SerializeField] private CheckCorrectTogglesActivate refCheckCorrectTogglesActivate;

        #endregion

        #region accesores

        public int _indexTransformadorSeleccionado
        {
            get { return indexTransformadorSeleccionado; }
        }

        public bool _zonaSeguridadColocada
        {
            set
            {
                zonaSeguridadColocada = value;
                buttonCelular.interactable = zonaSeguridadColocada && avatarVestido;

                if (buttonCelular.interactable)
                    refControladorCelular.SetNuevoMensajeUsuario();

                SetEtapaZonaSeguridad();
            }
            get { return zonaSeguridadColocada; }
        }

        public bool _avatarVestido
        {
            set
            {
                avatarVestido = value;
                buttonCelular.interactable = zonaSeguridadColocada && avatarVestido;

                if (buttonCelular.interactable)
                    refControladorCelular.SetNuevoMensajeUsuario();
            }
            get { return avatarVestido; }
        }

        public byte _mensajesEnviadosCentroControl
        {
            get { return mensajesEnviadosCentroControl; }
        }

        public float[] _corrienteCablesTransformador
        {
            get { return corrienteCablesTransformador; }
        }

        public float[] _corrienteCablesMacromedidor
        {
            get { return corrienteCablesMacromedidor; }
        }

        public float[] _tensionLineaPuntasPinza
        {
            get { return tensionLineaPuntasPinza; }
        }

        public float[] _tensionFasePuntasPinza
        {
            get { return tensionFasePuntasPinza; }
        }

        public float[] _tipoFusible
        {
            get { return tipoFusible; }
        }

        public float _potenciaTransformadorActual
        {
            get { return potenciaTransformadorActual; }
        }

        public EtapaSituacionActual _etapaSituacionActual
        {
            get { return etapaSituacionActual; }
        }

        #endregion

        private void Awake()
        {
            refCortaCircuitoDesconectadoRamdom.SetRamdomCanuelaDesconectada();
            SetTransformadorRamdom();
            CalcularValoresSistema();
        }

        #region private methods

        private void OnButtonAceptarMensajeInformacion()
        {
            refActiveZonesController.ActiveAllZones();
        }

        private void SetTransformadorRamdom()
        {
            indexTransformadorSeleccionado = Random.Range(0, arrayTransformadores.Length);
            imageTransformador.sprite = arrayTransformadores[indexTransformadorSeleccionado].spriteTransformador;
        }

        public void CalcularValoresSistema()
        {
            corrientePantallaMacroMedidor = 0;

            for (int i = 0; i < 3; i++)
            {
                if (i == refCortaCircuitoDesconectadoRamdom._ramdomIndexCanuelaDesconectada)
                    continue;

                potenciaTransformadorActual = (arrayTransformadores[indexTransformadorSeleccionado].potenciaKVA * 1000f);

                corrienteCablesTransformador[i] = (potenciaTransformadorActual / 360.26656f) * (Random.Range(0.975f, 1.025f) + 0.01f);
                corrienteCablesMacromedidor[i] = corrienteCablesTransformador[i] / factorDivisionSegunTransformador[indexTransformadorSeleccionado]; //gran variacion del 0.02%
                tensionLineaPuntasPinza[i] = potenciaTransformadorActual / (1.732050f * corrienteCablesTransformador[i]);
                tensionFasePuntasPinza[i] = tensionLineaPuntasPinza[i] / 1.732050f;
                potenciaMacroMedidor[i] = 1.732050f * tensionLineaPuntasPinza[i] * corrienteCablesMacromedidor[i] * 0.95f;
                corrientePantallaMacroMedidor += potenciaMacroMedidor[i];
            }

            //la punta de tension debe medir tambien en los cables del transformador
            textPantallaMacroMedidor.text = corrientePantallaMacroMedidor + " KWh";
        }

        #endregion

        #region public methods

        public void OnButtonZonaActivaTransformador()
        {
            refActiveZonesController.DesactiveAllZones();

            if (avatarVestido)
            {
                if (zonaSeguridadColocada)
                {
                    PanelInterfazTransformador._instance.Mostrar();
                    SetEtapaTransformador();
                }
                else
                    BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeSenializacionNoColocada"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"), OnButtonAceptarMensajeInformacion);
            }
            else
                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeVestimentaIncorrecta"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"), OnButtonAceptarMensajeInformacion);
        }

        public void OnButtonZonaActivaMacroMedidor()
        {
            refActiveZonesController.DesactiveAllZones();

            if (avatarVestido)
            {
                if (zonaSeguridadColocada)
                {
                    PanelInterfazMacroMedidor._instance.Mostrar();
                    SetEtapaMacroMedidor();
                }
                else
                    BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeSenializacionNoColocada"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"), OnButtonAceptarMensajeInformacion);
            }
            else
                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeVestimentaIncorrecta"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"), OnButtonAceptarMensajeInformacion);
        }

        public void OnButtonZonaActivaCortacircuito()
        {
            refActiveZonesController.DesactiveAllZones();

            if (avatarVestido)
            {
                if (!zonaSeguridadColocada)
                {
                    BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeSenializacionNoColocada"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"), OnButtonAceptarMensajeInformacion);
                    return;
                }

                var tmpValorFusible = refPanelInterfazRegistroDatos.GetFusibleValue();

                if (tmpValorFusible == -1)
                    BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeAsignarValorFusible"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"), OnButtonAceptarMensajeInformacion);
                else
                {
                    PanelInterfazCortaCircuitos._instance.Mostrar();
                    SetEtapaCortaCircutosMalo();
                }
            }
            else
                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeVestimentaIncorrecta"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"), OnButtonAceptarMensajeInformacion);
        }

        public void AgregarMensajesEnviados()
        {
            mensajesEnviadosCentroControl++;
        }

        public void OnButtonValidarHerramienta()
        {
            DesactivarHerramientas();

            switch (etapaSituacionActual)
            {
                case EtapaSituacionActual.Null:

                    BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeNoPuedeUsarHerramientaAqui"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"), OnButtonAceptarMensajeInformacion);
                    PanelInterfazCajaHerramientas._instance.Mostrar(false);

                    break;

                case EtapaSituacionActual.VestirAvatar:

                    BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeNoPuedeUsarHerramientaAqui"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"), OnButtonAceptarMensajeInformacion);
                    PanelInterfazCajaHerramientas._instance.Mostrar(false);

                    break;

                case EtapaSituacionActual.ZonaSeguridad:

                    Debug.Log("EtapaSituacionActual.ZonaSeguridad");

                    if (refCheckCorrectTogglesActivate.VerifyGroupToggles("zonaSeguridad"))
                    {
                        escalera.SetActive(true);
                        imageEscalera.SetActive(false);
                        escaleraCamioneta.SetActive(false);
                        PanelInterfazCajaHerramientas._instance.Mostrar(false);
                        refActiveZonesController.ActiveAllZones();
                    }
                    else if (refCheckCorrectTogglesActivate.AnyToggleActive())
                        BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeHerramientaInadecuada"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                    else
                        BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeHerramientaNoSeleccionada"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));

                    break;

                case EtapaSituacionActual.MacroMedidor:

                    Debug.Log("EtapaSituacionActual.MacroMedidor");

                    if (refCheckCorrectTogglesActivate.VerifyGroupToggles("macroMedidor"))
                    {
                        pinzaAmperimetro.SetActive(true);
                        PanelInterfazCajaHerramientas._instance.Mostrar(false);
                    }
                    else if (refCheckCorrectTogglesActivate.AnyToggleActive())
                        BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeHerramientaInadecuada"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                    else
                        BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeHerramientaNoSeleccionada"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));

                    break;

                case EtapaSituacionActual.Transformador:

                    Debug.Log("EtapaSituacionActual.Transformador");

                    if (refCheckCorrectTogglesActivate.VerifyGroupToggles("transformador") && refCheckCorrectTogglesActivate.VerifyGroupToggles("cortaCircuitos"))
                        BoxMessageManager._instance.MtdCreateBoxMessageInfo("No puede seleccionar tantas herramientas a la vez.", DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                    else if (refCheckCorrectTogglesActivate.VerifyGroupToggles("cortaCircuitos"))
                    {
                        pinzaAmperimetro.SetActive(false);
                        puntaDetectora.SetActive(true);
                        PanelInterfazCajaHerramientas._instance.Mostrar(false);
                    }
                    else if (refCheckCorrectTogglesActivate.VerifyGroupToggles("transformador"))
                    {
                        pinzaAmperimetro.SetActive(true);
                        puntaDetectora.SetActive(false);
                        PanelInterfazCajaHerramientas._instance.Mostrar(false);
                    }
                    else if (refCheckCorrectTogglesActivate.AnyToggleActive())
                        BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeHerramientaInadecuada"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                    else
                        BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeHerramientaNoSeleccionada"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));

                    break;

                case EtapaSituacionActual.SeleccionCortaCircutoMalo:

                    Debug.Log("EtapaSituacionActual.SeleccionCortaCircutoMalo");

                    if (refCheckCorrectTogglesActivate.VerifyGroupToggles("cortaCircuitos"))
                    {
                        //pinzaAmperimetro.SetActive(true);
                        puntaDetectora.SetActive(true);
                        //letreroNoOperar.SetActive(true);
                        PanelInterfazCajaHerramientas._instance.Mostrar(false);
                    }
                    else if (refCheckCorrectTogglesActivate.AnyToggleActive())
                        BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeHerramientaInadecuada"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                    else
                        BoxMessageManager._instance.MtdCreateBoxMessageInfo("No ha seleccionadoninguna herramienta.", DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));

                    break;

                case EtapaSituacionActual.VisualizacionCortaCircuitoMalo:

                    if (refCheckCorrectTogglesActivate.VerifyGroupToggles("visualizacionCortaCircuitosMalo"))
                    {
                        bastonPerdiga.SetActive(true);
                        PanelInterfazCajaHerramientas._instance.Mostrar(false);
                    }
                    else if (refCheckCorrectTogglesActivate.AnyToggleActive())
                        BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeHerramientaInadecuada"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                    else
                        BoxMessageManager._instance.MtdCreateBoxMessageInfo("No ha seleccionadoninguna herramienta.", DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));

                    break;
            }
        }

        public void DesactivarHerramientas()
        {
            puntaDetectora.SetActive(false);
            pinzaAmperimetro.SetActive(false);
            bastonPerdiga.SetActive(false);
            //letreroNoOperar.SetActive(false);
        }

        //set Zones
        public void SetEtapaVestirAvatar()
        {
            Debug.Log("SetEtapaVestirAvatar");
            etapaSituacionActual = EtapaSituacionActual.VestirAvatar;
        }

        public void SetEtapaZonaSeguridad()
        {
            Debug.Log("SetEtapaZonaSeguridad");
            etapaSituacionActual = EtapaSituacionActual.ZonaSeguridad;
        }

        public void SetEtapaMacroMedidor()
        {
            Debug.Log("SetEtapaMacroMedidor");
            etapaSituacionActual = EtapaSituacionActual.MacroMedidor;
            puntaDetectora.SetActive(false);
            //escalera.SetActive(false);
            pinzaAmperimetro.SetActive(false);
        }

        public void SetEtapaVisualizacionCortaCircuito()
        {
            Debug.Log("SetEtapaVisualizacionCortaCircuito");
            etapaSituacionActual = EtapaSituacionActual.VisualizacionCortaCircuitoMalo;
            puntaDetectora.SetActive(false);
            //escalera.SetActive(false);
            pinzaAmperimetro.SetActive(false);
        }

        public void SetEtapaCortaCircutosMalo()
        {
            Debug.Log("SetEtapaCortaCircuto");
            etapaSituacionActual = EtapaSituacionActual.SeleccionCortaCircutoMalo;
            puntaDetectora.SetActive(false);
            //escalera.SetActive(false);
            pinzaAmperimetro.SetActive(false);
        }

        public void SetEtapaTransformador()
        {
            Debug.Log("SetEtapaTransformador");
            etapaSituacionActual = EtapaSituacionActual.Transformador;
            puntaDetectora.SetActive(false);
            //escalera.SetActive(false);
            pinzaAmperimetro.SetActive(false);
        }
        #endregion
    }

    [Serializable]
    public class Transformador
    {
        public float potenciaKVA;

        public Sprite spriteTransformador;
    }

    public enum EtapaSituacionActual
    {
        Null,
        VestirAvatar,
        ZonaSeguridad,
        MacroMedidor,
        VisualizacionCortaCircuitoMalo,
        SeleccionCortaCircutoMalo,
        Transformador
    }
}