﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace NSUtilities
{
    public class DetencionObjetoCerca : MonoBehaviour
    {
        #region members

        [SerializeField]
        private RectTransform rectTransformObjetivo;
        
        [SerializeField]
        private RectTransform[] arrayRectTransformObjetivo;

        [SerializeField]
        private RectTransform rectTransformThis;

        [SerializeField]
        private float distanciaMinima;

        [SerializeField]
        private bool ejecutarCheckeo;

        private bool notificarObjetoCerca;

        /// <summary>
        /// Para saber que objeto del array de posibles objetos cerca, es el que notifico la cercania
        /// </summary>
        private int indexObjetoCerca = -1;
        #endregion

        #region events

        public UnityEvent OnObjetoCerca;

        public UnityEventInt OnSetIndexCableConectado;
        #endregion

        // Update is called once per frame
        private void Update()
        {
            CheckObjetoCerca();
            CheckArrayObjetosCerca();
        }

        private void CheckObjetoCerca()
        {
            if (!rectTransformObjetivo)
                return;
            
            if (!ejecutarCheckeo)
                return;
            
            if (!rectTransformObjetivo.gameObject.activeSelf)
                return;

            if (Vector3.Distance(rectTransformThis.position, rectTransformObjetivo.position) <= distanciaMinima)
            {
                if (notificarObjetoCerca)
                {
                    OnObjetoCerca.Invoke();
                    notificarObjetoCerca = false;
                }
            }
            else
                notificarObjetoCerca = true;
        }

        private void CheckArrayObjetosCerca()
        {
            if (!ejecutarCheckeo)
                return;

            if (CheckObjetoArrayCerca())
            {
                if (notificarObjetoCerca)
                {
                    OnObjetoCerca.Invoke();
                    OnSetIndexCableConectado.Invoke(indexObjetoCerca);
                    notificarObjetoCerca = false;
                }
            }
            else
                notificarObjetoCerca = true;
        }
        
        private bool CheckObjetoArrayCerca()
        {
            for (int i = 0; i < arrayRectTransformObjetivo.Length; i++)
            {
                if (Vector3.Distance(rectTransformThis.position, arrayRectTransformObjetivo[i].position) <= distanciaMinima)
                {
                    indexObjetoCerca = i;
                    return true;
                }
            }
            
            return false;
        }
    }

    [Serializable]
    public class UnityEventInt : UnityEvent<int>
    {
        
    }
}