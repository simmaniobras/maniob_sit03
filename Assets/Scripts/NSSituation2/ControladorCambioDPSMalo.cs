﻿using NSBoxMessage;
using NSTraduccionIdiomas;
using UnityEngine;
using UnityEngine.Events;

namespace NSSituacion2
{
    public class ControladorCambioDPSMalo : MonoBehaviour
    {
        #region members
        
        public UnityEvent OnDPSCanBeChanged;

        public UnityEvent OnDPSCantBeChanged;

        public UnityEvent OnDPSNoPuedenSerCambiadosTensionAleatoria;

        public UnityEvent OnArnesNoAsegurado;
        
        [SerializeField] private GameObject goLlaveInglesa;

        [SerializeField] private ControladorTensionTransformador refControladorTensionTransformador;

        [SerializeField] private GameObject fusiblesDescolgados;

        [SerializeField] private ControladorSituacion2 refControladorSituacion2;
        
        #endregion

        public void ChangeDPS()
        {
            if (!goLlaveInglesa.activeSelf)
                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeHerramientaIncorrectaCambioDPS"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
            
            if (!refControladorSituacion2.ArnesAsegurado)
            {
                OnArnesNoAsegurado.Invoke();
                return;
            }
            
            if (refControladorTensionTransformador._tensionAleatoriaActivada)
            {
                OnDPSNoPuedenSerCambiadosTensionAleatoria.Invoke();
                return;
            }
            
            if (fusiblesDescolgados.activeSelf)
            {
                OnDPSCanBeChanged.Invoke();
                return;
            }
            
            OnDPSCantBeChanged.Invoke();
        }
    }
}