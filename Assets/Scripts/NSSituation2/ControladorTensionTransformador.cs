﻿using NSPuntaDetectora;
using UnityEngine;
using UnityEngine.UI;

namespace NSSituacion2
{
    public class ControladorTensionTransformador : MonoBehaviour
    {
        [SerializeField] private PuntoMedicionPuntaDetectora[] arraysPuntosMedicionAltaTension;

        [SerializeField] private PuntoMedicionPuntaDetectora[] arraysPuntosMedicionBajaTension;

        private bool tensionAleatoriaActivada;

        public bool _tensionAleatoriaActivada
        {
            get
            {
                return tensionAleatoriaActivada;
            }
        }

        public void DesactivarTensionAleatorio()
        {
            foreach (var puntoMedicion in arraysPuntosMedicionAltaTension)
                puntoMedicion._tieneTension = false;

            foreach (var puntoMedicion in arraysPuntosMedicionBajaTension)
            {
                if (Random.value <= 0.15f)
                {
                    puntoMedicion._tieneTension = true;
                    //tensionAleatoriaActivada = true;
                }
                else
                    puntoMedicion._tieneTension = false;
            }
        }

        public void DesactivarTension()
        {
            foreach (var puntoMedicion in arraysPuntosMedicionAltaTension)
                puntoMedicion._tieneTension = false;

            foreach (var puntoMedicion in arraysPuntosMedicionBajaTension)
                puntoMedicion._tieneTension = false;
            
            tensionAleatoriaActivada = false;
        }

        public void ActivarTension()
        {
            foreach (var puntoMedicion in arraysPuntosMedicionAltaTension)           
                puntoMedicion._tieneTension = true;   
            
            foreach (var puntoMedicion in arraysPuntosMedicionBajaTension)
                puntoMedicion._tieneTension = true;
        }
    }
}