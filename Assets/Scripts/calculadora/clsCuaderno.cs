﻿using NSCreacionPDF;
using NSTraduccionIdiomas;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Serialization;

public class clsCuaderno : MonoBehaviour
{
    #region members

    public TextMeshProUGUI textPregunta;
    
    public TMP_InputField cuaderno;
    
    public TextMeshProUGUI TextoPaginaActual;
    
    public int NumeroDeCaracteres;
    
    public string[] preguntasComplementariasSituacion;
    
    public ControladorValoresPDF pdfManger;
    
    public Sprite[] botonEliminar;
    
    public Sprite[] botonHojaNUeva;

    private List<string> HojasNuevas;

    [SerializeField] private Button BTeleiminar;
    
    [SerializeField] private Button BTNuevaHoja;

    private List<string> preguntasDeMas;
    
    private List<string> ListRespuestas;
    
    private int HojaActual;
    
    private int numPreguntas;

    private bool hanEscritoRespuestas = false;  

    private int OjasENMemoria = 4;

    #endregion

    #region monoBehaviour

    private void Awake()
    {
        InitCuaderno();
    }

    // Update is called once per frame
    private void Update()
    {
        TextoPaginaActual.text = "Página " + (HojaActual + 1);

        if ((ListRespuestas.Count <= HojaActual + 1) && HojaActual == (ListRespuestas.Count - 1) + HojasNuevas.Count)
        {
            if (HojasNuevas.Count < OjasENMemoria)
            {
                BTNuevaHoja.gameObject.GetComponent<Image>().sprite = botonHojaNUeva[1];
                BTNuevaHoja.enabled = true;
            }
        }
        else
        {
            BTNuevaHoja.gameObject.GetComponent<Image>().sprite = botonHojaNUeva[0];
            BTNuevaHoja.enabled = false;
        }

        if (ListRespuestas.Count <= HojaActual && HojasNuevas.Count >= 1)
        {
            BTeleiminar.GetComponent<Image>().sprite = botonEliminar[1];
            BTeleiminar.enabled = true;
        }

        else
        {
            BTeleiminar.GetComponent<Image>().sprite = botonEliminar[0];
            BTeleiminar.enabled = false;
        }
    }

    #endregion

    #region private methods

    /// <summary>
    /// funsion que inicialisa la lista de preguntas
    /// </summary>
    private void MtdInitRespuestas()
    {
        mtdllenarlista(preguntasComplementariasSituacion);
    }

    /// <summary>
    /// Inicializa la lista que contendra las respuestas del usuario
    /// </summary>
    /// <param name="preguntas"></param>
    private void mtdllenarlista(string[] preguntas)
    {
        ListRespuestas.Clear();
        numPreguntas = 0;
        for (int i = 0; i < preguntas.Length; i++)
        {
            numPreguntas++;
            ListRespuestas.Add("");
        }
    }

    /// <summary>
    /// pone el texto de respues correspondiente a cada pregunta 
    /// </summary>
    private void mtdActualisarRespuesta()
    {
        if (HojaActual < ListRespuestas.Count)
            cuaderno.text = ListRespuestas[HojaActual];
        else
            cuaderno.text = HojasNuevas[HojaActual - ListRespuestas.Count];
    }


    //---------------------------------------------------------------------------------------

    /// <summary>
    ///  pinta la hoja del cuaderno actual con la pregunta correspondiente
    /// </summary>
    private void mtdPintarPregunta()
    {
        if (HojaActual < preguntasComplementariasSituacion.Length)        
            textPregunta.text = DiccionarioIdiomas._instance.Traducir(preguntasComplementariasSituacion[HojaActual]);       
        else
            textPregunta.text = "";
    }

    private void MtdReposicionarInput()
    {
        Debug.Log("tamaño de las preguntas" + textPregunta.text.Length);

        var tam = textPregunta.text.Length;

        if (tam > NumeroDeCaracteres)
        {
            if (tam > NumeroDeCaracteres * 2)
            {
                if (tam > NumeroDeCaracteres * 3)
                {

                }
                else
                    cuaderno.GetComponent<RectTransform>().sizeDelta = new Vector2(1291.3f, 478);
            }
            else
                cuaderno.GetComponent<RectTransform>().sizeDelta = new Vector2(1291.3f, 524);
        }
        else
        {
            if (tam <= 1)
                cuaderno.GetComponent<RectTransform>().sizeDelta = new Vector2(1291.3f, 615);
            else
                cuaderno.GetComponent<RectTransform>().sizeDelta = new Vector2(1291.3f, 569);
        }
    }

    #endregion

    #region public methods

    /// <summary>
    /// inicializa el cuaderno 
    /// </summary>
    public void InitCuaderno()
    {
        HojasNuevas = new List<string>();
        numPreguntas = 0;
        HojaActual = 0;
        ListRespuestas = new List<string>();

        Debug.Log("ya tengo cargada la matriz");
        cuaderno.text = "";
        MtdInitRespuestas();
        mtdPintarPregunta();
        MtdReposicionarInput();
    }


    /// <summary>
    /// guarda cada vez que hay un cambio en el texto de respuesta 
    /// </summary>
    public void mtdguardarCambios()
    {
        Debug.Log("entre a guardar");
        int tamActual = cuaderno.text.Length;
        int tamAnterior = 0;
        hanEscritoRespuestas = true;

        if (HojaActual < ListRespuestas.Count)
            tamAnterior = ListRespuestas[HojaActual].Length;

        if (HojaActual < ListRespuestas.Count)
        {
            if (tamActual != tamAnterior)
                ListRespuestas[HojaActual] = cuaderno.text;
        }
        else
            HojasNuevas[HojaActual - ListRespuestas.Count] = cuaderno.text;
    }


    public void btnAdelante()
    {
        if (numPreguntas + HojasNuevas.Count - 1 == HojaActual)
            Debug.Log("no hay mas hojas");
        else
        {
            HojaActual++;
            mtdPintarPregunta();
            mtdActualisarRespuesta();
        }

        MtdReposicionarInput();
    }

    public void btnAtras()
    {
        if (0 == HojaActual)
            Debug.Log("no hay mas hojas");
        else
        {
            HojaActual = HojaActual - 1;
            mtdPintarPregunta();
            mtdActualisarRespuesta();
        }

        MtdReposicionarInput();
    }


    public void reiniciarCuaderno()
    {
        cuaderno.text = "";
        MtdInitRespuestas();
        MtdReposicionarInput();
        HojasNuevas.Clear();
    }

    public void actualizarCuaderno()
    {
        MtdReposicionarInput();
    }

    /// <summary>
    /// metodo que pasa los valoresd de las preguntas al pdf 
    /// </summary>
    public void mtdPasarInfoAlPdf()
    {
        bool HayHojasCreadas = false;
        string[] PregRespuesta;

        if (HojasNuevas.Count > 0)
        {
            PregRespuesta = new string[ListRespuestas.Count + HojasNuevas.Count];
            HayHojasCreadas = true;
        }
        else
            PregRespuesta = new string[ListRespuestas.Count];


        for (int i = 0; i < ListRespuestas.Count; i++)       
            PregRespuesta[i] = DiccionarioIdiomas._instance.Traducir(preguntasComplementariasSituacion[i]) + "\n" + ListRespuestas[i];        

        if (HayHojasCreadas)
        {
            for (int i = 0; i < HojasNuevas.Count; i++)
                PregRespuesta[i + ListRespuestas.Count] = HojasNuevas[i];
        }

        if (pdfManger)
            pdfManger.SetPreguntasCuaderno(PregRespuesta, hanEscritoRespuestas);
    }

    public void mtdCrearNuevaHojas()
    {
        string aux = "";
        HojasNuevas.Add(aux);
        HojaActual = HojasNuevas.Count + (ListRespuestas.Count - 1);
        mtdPintarPregunta();
        mtdActualisarRespuesta();
        MtdReposicionarInput();
    }

    public void mtdEliminarHojaNueva()
    {
        HojasNuevas.RemoveAt(HojaActual - ListRespuestas.Count);
        HojaActual--;
        mtdPintarPregunta();
        mtdActualisarRespuesta();
        MtdReposicionarInput();
    }
    #endregion
}