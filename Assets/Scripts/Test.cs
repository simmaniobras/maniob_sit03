﻿using NSActiveZones;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSTest
{
    public class Test : MonoBehaviour
    {
        [SerializeField]
        private ActiveZonesController refActiveZonesController;

        private void Start()
        {
            refActiveZonesController.DesactivateZone("transformer");
            refActiveZonesController.DesactivateZone("electricbox");
            refActiveZonesController.DesactivateZone("cortacircuito");
        }
    }
}