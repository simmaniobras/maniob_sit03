﻿using NSSituacion1;
using NSSituacion2;
using NSSituacion3;
using NSSituacion5;
using NSSituacionGeneral;
using NSUtilities;
using UnityEngine;


namespace NSPinzaVoltaje
{
    public class PuntoMedicion : MonoBehaviour
    {
        #region members

        [SerializeField] private bool medirCablesSituacion3;

        [SerializeField]
        private bool medirCorrienteCablesTransformador;

        [SerializeField]
        private bool medirCorrienteCablesMacromedidor;

        [SerializeField]
        private bool medirTensionLineaPuntasPinza;

        [SerializeField]
        private bool medirTensionFasePuntasPinza;

        [SerializeField]
        private int indexMedicion;

        private bool estaSiendoUsado;

        [SerializeField]
        private Vector3 posicionDeDesface;

        [SerializeField]
        private float scalePinza = 1;

        [SerializeField]
        private float anguloAgarrePinza = 20;

        [SerializeField]
        private ImageAnimationFade refImageAnimationFade;

        [SerializeField] private AbstractControladorSituacion refAbstractControladorSituacion;

        [SerializeField]
        private ControladorSituacion1 refControladorSituacion1;

        [SerializeField]
        private ControladorSituacion2 refControladorSituacion2;

        [SerializeField]
        private ControladorSituacion3 refControladorSituacion3;

        [SerializeField]
        private ControladorSituacion4 refControladorSituacion4;

        [SerializeField]
        private ControladorSituacion5 refControladorSituacion5;

        [SerializeField]
        private GameObject remplazoCable;
        #endregion

        #region accesor

        public float _valorMedicion
        {
            get
            {
                if (refAbstractControladorSituacion)
                    if (refAbstractControladorSituacion.CortaCircuitosAbiertos)
                        return 0f;

                if (medirCorrienteCablesTransformador)
                {
                    if (refControladorSituacion1)
                        return refControladorSituacion1._corrienteCablesTransformador[indexMedicion];
                    
                    if (refControladorSituacion2)
                        return refControladorSituacion2._corrienteCablesTransformador[indexMedicion];
                    
                    if (refControladorSituacion3)
                    {
                        if (medirCablesSituacion3)   
                            return refControladorSituacion3.GetCorrienteCablesTransformadorSituacion3(indexMedicion);

                        return refControladorSituacion3._corrienteCablesTransformador[indexMedicion];    
                    }
                    if (refControladorSituacion4)
                        return refControladorSituacion4._corrienteCablesTransformador[indexMedicion];

                    if (refControladorSituacion5)
                            return refControladorSituacion5._corrienteCablesTransformador[indexMedicion];
                }

                if (medirCorrienteCablesMacromedidor)
                {
                    if (refControladorSituacion1)
                        return refControladorSituacion1._corrienteCablesMacromedidor[indexMedicion];
                    
                    if (refControladorSituacion2)
                        return refControladorSituacion2._corrienteCablesMacromedidor[indexMedicion];
                    
                    if (refControladorSituacion3)
                        return refControladorSituacion3._corrienteCablesMacromedidor[indexMedicion];

                    if (refControladorSituacion4)
                        return refControladorSituacion4._corrienteCablesMacromedidor[indexMedicion];

                    if (refControladorSituacion5)
                        return refControladorSituacion5._corrienteCablesMacromedidor[indexMedicion];
                }

                if (medirTensionLineaPuntasPinza)
                {
                    if (refControladorSituacion1)
                        return refControladorSituacion1._tensionLineaPuntasPinza[indexMedicion];
                    
                    if (refControladorSituacion2)
                        return refControladorSituacion2._tensionLineaPuntasPinza[indexMedicion];
                    
                    if (refControladorSituacion3)
                    {
                        if (medirCablesSituacion3)
                            return refControladorSituacion3.GetTensionLineaPuntasPinzaSituacion3(indexMedicion);

                        return refControladorSituacion3._tensionLineaPuntasPinza[indexMedicion];
                    }
                    if (refControladorSituacion4)
                        return refControladorSituacion4._tensionLineaPuntasPinza[indexMedicion];

                    if (refControladorSituacion5)
                        return refControladorSituacion5._tensionLineaPuntasPinza[indexMedicion];
                }

                if (medirTensionFasePuntasPinza)
                {
                    if (refControladorSituacion1)
                        return refControladorSituacion1._tensionFasePuntasPinza[indexMedicion];
                    
                    if (refControladorSituacion2)
                        return refControladorSituacion2._tensionFasePuntasPinza[indexMedicion];
                    
                    if (refControladorSituacion3)
                    {
                        if (medirCablesSituacion3)
                            return refControladorSituacion3.GetTensionFasePuntasPinzaSituacion3(indexMedicion);

                        return refControladorSituacion3._tensionFasePuntasPinza[indexMedicion];
                    }
                    if (refControladorSituacion4)
                        return refControladorSituacion4._tensionFasePuntasPinza[indexMedicion];

                    if (refControladorSituacion5)
                        return refControladorSituacion5._tensionFasePuntasPinza[indexMedicion];
                }
                return 1;
            }
        }

        public bool _estaSiendoUsado
        {
            set
            {
                estaSiendoUsado = value;
                gameObject.SetActive(!estaSiendoUsado);
            }
            get
            {
                return estaSiendoUsado;
            }
        }

        public Vector3 _posicionDeDesface
        {
            get
            {
                return posicionDeDesface;
            }
        }

        public float _scalePinza
        {
            get
            {
                return scalePinza;
            }
        }

        public int _indexMedicion
        {
            get
            {
                return indexMedicion;
            }
        }

        public float _anguloAgarrePinza
        {
            get
            {
                return anguloAgarrePinza;
            }
        }
        #endregion

        #region public methods

        public void SetMedicionesPuntaNegra()
        {
            medirCorrienteCablesTransformador = false;
            medirCorrienteCablesMacromedidor = false;
            medirTensionLineaPuntasPinza = true;
            medirTensionFasePuntasPinza = false;
        }

        public void SetMedicionesPuntaRoja()
        {
            medirCorrienteCablesTransformador = false;
            medirCorrienteCablesMacromedidor = false;
            medirTensionLineaPuntasPinza = false;
            medirTensionFasePuntasPinza = true;
        }

        public void ActivarCableRemplazo(bool argActivar = true)
        {
            if (remplazoCable)
                remplazoCable.SetActive(argActivar);
        }
        #endregion
    }
}