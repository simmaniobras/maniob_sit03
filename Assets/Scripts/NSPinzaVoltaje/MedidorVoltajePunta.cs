﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

namespace NSPinzaVoltaje
{
    public class MedidorVoltajePunta : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        #region member

        private RectTransform rectTransform;

        private RectTransform rectTransformPuntoMedicionActual;

        private Vector3 posicionPuntero;

        private Vector3 posicionInicial;

        [SerializeField] private float minDistanceToPoint = 10;

        private List<GameObject> puntosMedicionCerca = new List<GameObject>();

        [SerializeField] private TextMeshProUGUI textVoltajePantalla;

        [SerializeField] private TextMeshProUGUI textVoltajePantallaPinza;

        [SerializeField] private bool soyPuntaRoja;

        [SerializeField] private MedidorVoltajePunta refMedidorVoltajePuntaNegra;

        [SerializeField] private MedidorVoltajePunta refMedidorVoltajePuntaRoja;

        [SerializeField] private int indexMedicion;

        private float valorMedicionActual;

        [SerializeField] private GameObject[] zonasMedicion;

        [SerializeField] private LineRenderer lineRenderCable;

        [SerializeField] private RectTransform rectTransformPivotePunta;

        [SerializeField] private RectTransform rectTransformPivotePinza;

        [SerializeField] private RectTransform rectTransformCanvas;

        [SerializeField] private Camera cameraRenderCables;

        //[SerializeField] private float tmpP;

        #endregion

        #region accesors

        public int _indexMedicion
        {
            get { return indexMedicion; }
        }

        public float _valorMedicionActual
        {
            get { return valorMedicionActual; }
        }

        #endregion

        private void Awake()
        {
            rectTransform = gameObject.GetComponent<RectTransform>();
            posicionInicial = rectTransform.position;
            posicionPuntero = posicionInicial;
        }

        private void OnEnable()
        {
            lineRenderCable.gameObject.SetActive(true);

            if (rectTransformPuntoMedicionActual)
                rectTransformPuntoMedicionActual.GetComponent<PuntoMedicion>()._estaSiendoUsado = false;

            rectTransformPuntoMedicionActual = null;
            rectTransform.position = posicionInicial;
        }

        private void OnDisable()
        {
            lineRenderCable.gameObject.SetActive(false);
        }

        private void Update()
        {
            MoverseHaciaPuntoMedicionVoltaje();
            //DrawCable();
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            ActivarZonasMedicion();

            if (rectTransformPuntoMedicionActual)
                rectTransformPuntoMedicionActual.GetComponent<PuntoMedicion>()._estaSiendoUsado = false;

            GetPuntosMedicionCerca();
        }

        public void OnDrag(PointerEventData eventData)
        {
            var tmpPuntoVoltaje = RevisarPuntoCerca(eventData.position);

            if (tmpPuntoVoltaje)
            {
                if (rectTransformPuntoMedicionActual)
                {
                    rectTransformPuntoMedicionActual.GetComponent<PuntoMedicion>()._estaSiendoUsado = false;
                    rectTransformPuntoMedicionActual = tmpPuntoVoltaje;
                    rectTransformPuntoMedicionActual.GetComponent<PuntoMedicion>()._estaSiendoUsado = true;
                    Debug.Log("hay punto de medicion  y me muevo hacia el: " + rectTransformPuntoMedicionActual);
                }
                else
                {
                    rectTransformPuntoMedicionActual = tmpPuntoVoltaje;
                    rectTransformPuntoMedicionActual.GetComponent<PuntoMedicion>()._estaSiendoUsado = true;
                    Debug.Log("hay punto de medicion nuevo : " + rectTransformPuntoMedicionActual);
                }
            }
            else
            {
                if (rectTransformPuntoMedicionActual)
                {
                    rectTransformPuntoMedicionActual.GetComponent<PuntoMedicion>()._estaSiendoUsado = false;
                    rectTransformPuntoMedicionActual = null;
                    Debug.Log("no hay punto de medicion cerca : " + rectTransformPuntoMedicionActual);
                }
            }

            posicionPuntero = eventData.position;
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            posicionPuntero = posicionInicial;
            ActivarZonasMedicion(false);
        }

        private void MoverseHaciaPuntoMedicionVoltaje()
        {
            if (rectTransformPuntoMedicionActual)
            {
                rectTransform.position = Vector3.Lerp(rectTransform.position, rectTransformPuntoMedicionActual.position, 0.2f);
                var tmpPuntoMedicion = rectTransformPuntoMedicionActual.GetComponent<PuntoMedicion>();
                indexMedicion = tmpPuntoMedicion._indexMedicion;

                if (indexMedicion >= 0)
                    valorMedicionActual = tmpPuntoMedicion._valorMedicion;
                else
                    valorMedicionActual = 1;

                if (soyPuntaRoja)
                {
                    if (refMedidorVoltajePuntaNegra._indexMedicion == -1)
                    {
                        if (indexMedicion != refMedidorVoltajePuntaNegra._indexMedicion)
                        {
                            tmpPuntoMedicion.SetMedicionesPuntaRoja();

                            if (refMedidorVoltajePuntaNegra._valorMedicionActual == 0)
                            {
                                Debug.Log("Midiendo aqui 1");
                                textVoltajePantallaPinza.text = "0";
                                textVoltajePantalla.text = "0";
                            }
                            else
                            {
                                Debug.Log("Midiendo aqui 2");
                                textVoltajePantallaPinza.text = rectTransformPuntoMedicionActual.GetComponent<PuntoMedicion>()._valorMedicion.ToString("0.000");
                                textVoltajePantalla.text = rectTransformPuntoMedicionActual.GetComponent<PuntoMedicion>()._valorMedicion.ToString("0.000");
                            }
                        }
                        else
                        {
                            Debug.Log("Midiendo aqui 3");
                            textVoltajePantallaPinza.text = "0";
                            textVoltajePantalla.text = "0";
                        }
                    }
                    else
                    {
                        tmpPuntoMedicion.SetMedicionesPuntaNegra();
                        Debug.Log("indexMedicion : " + indexMedicion);

                        if (indexMedicion != -1 && indexMedicion != refMedidorVoltajePuntaNegra._indexMedicion)
                        {
                            if (refMedidorVoltajePuntaNegra._valorMedicionActual == 0)
                            {
                                textVoltajePantallaPinza.text = "0";
                                textVoltajePantalla.text = "0";
                            }
                            else
                            {
                                textVoltajePantallaPinza.text = rectTransformPuntoMedicionActual.GetComponent<PuntoMedicion>()._valorMedicion.ToString("0.000");
                                textVoltajePantalla.text = rectTransformPuntoMedicionActual.GetComponent<PuntoMedicion>()._valorMedicion.ToString("0.000");
                            }
                        }
                        else
                        {
                            tmpPuntoMedicion.SetMedicionesPuntaRoja();
                            textVoltajePantallaPinza.text = refMedidorVoltajePuntaNegra._valorMedicionActual.ToString("0.000");  // rectTransformPuntoMedicionActual.GetComponent<PuntoMedicion>()._valorMedicion.ToString("0.000");
                            textVoltajePantalla.text = refMedidorVoltajePuntaNegra._valorMedicionActual.ToString("0.000"); // rectTransformPuntoMedicionActual.GetComponent<PuntoMedicion>()._valorMedicion.ToString("0.000");

                            //textVoltajePantallaPinza.text = "0";
                            //textVoltajePantalla.text = "0";
                        }
                    }
                }
                else
                {
                    if (refMedidorVoltajePuntaRoja._indexMedicion == -1)
                        tmpPuntoMedicion.SetMedicionesPuntaRoja();
                }
            }
            else
            {
                rectTransform.position = Vector3.Lerp(rectTransform.position, posicionPuntero, 0.5f);
                textVoltajePantalla.text = "0";
                textVoltajePantallaPinza.text = "0";
                indexMedicion = -2;
                valorMedicionActual = 0;
            }
        }

        private RectTransform RevisarPuntoCerca(Vector3 argPosition)
        {
            foreach (var tmpPoint in puntosMedicionCerca)
            {
                var tmpPointRectTransform = tmpPoint.GetComponent<RectTransform>();

                if (Vector2.Distance(argPosition, tmpPointRectTransform.position) <= minDistanceToPoint)
                    return tmpPointRectTransform;
            }

            return null;
        }

        private void ActivarZonasMedicion(bool argActivar = true)
        {
            foreach (var tmpZonaMedicion in zonasMedicion)
                if (tmpZonaMedicion)
                    tmpZonaMedicion.SetActive(argActivar);
        }

        private void GetPuntosMedicionCerca()
        {
            puntosMedicionCerca.Clear();

            var tmpPuntosMedicionCerca = GameObject.FindGameObjectsWithTag("PuntosMedicionVoltaje");

            foreach (var tmpPuntoMedicionCerca in tmpPuntosMedicionCerca)
                if (!tmpPuntoMedicionCerca.GetComponent<PuntoMedicion>()._estaSiendoUsado)
                    puntosMedicionCerca.Add(tmpPuntoMedicionCerca);
        }

        private void DrawCable()
        {
            Vector3 tmpPositionPivotPinza = Camera.main.ScreenToWorldPoint(rectTransformPivotePinza.position);
            Vector3 tmpPositionPivotPunta = Camera.main.ScreenToWorldPoint(rectTransformPivotePunta.position);

            tmpPositionPivotPinza[2] = 0;
            tmpPositionPivotPunta[2] = 0;

            List<Vector3> tmpListPositions = new List<Vector3>();
            /*
             //(x-h) ^ 2 = 4p (y-gravityForEachSubdivision); formula parabola
             //h = puntoPinza[1]^2 - puntoPunta[1]^2 / (2*puntoPinza[1] + 2*puntoPunta[1]);
             //gravityForEachSubdivision = -(((x - h) ^ 2 ) / 4p) + y
             var tmpP = 5f;
             var tmpH = (Mathf.Pow(tmpPositionPivotPinza[0], 2) - Mathf.Pow(tmpPositionPivotPunta[0], 2)) / (2 * tmpPositionPivotPinza[0] - 2* tmpPositionPivotPunta[0]);
             var tmpK = -(Mathf.Pow(tmpPositionPivotPinza[0] - tmpH, 2)/ tmpP) + tmpPositionPivotPinza[1]; 

             for (float x = tmpPositionPivotPinza[0]; x <= tmpPositionPivotPunta[0]; x += 0.5f)
                 tmpListPositions.Add(new Vector3(x, (Mathf.Pow(x - tmpH, 2f) / tmpP) + tmpK)); // ((x-h)^2  /  4p ) + gravityForEachSubdivision = y

             tmpListPositions.Add(new Vector3(tmpPositionPivotPunta[0], tmpPositionPivotPunta[1])); // ((x-h)^2  /  4p ) + gravityForEachSubdivision = y
             */

            var tmpVectorDirection = tmpPositionPivotPunta - tmpPositionPivotPinza;
            var tmpTamanioSegmento = tmpVectorDirection.magnitude / 0.25f;

            Debug.Log(tmpTamanioSegmento);

            for (int i = 0; i < tmpTamanioSegmento; i++)
            {
               tmpListPositions.Add((tmpPositionPivotPinza + (tmpVectorDirection / tmpTamanioSegmento) * i) + Quaternion.Euler(0, 90, 0) * tmpVectorDirection.normalized * (Vector2.Distance(tmpPositionPivotPinza + (tmpVectorDirection / tmpTamanioSegmento) * i, tmpPositionPivotPinza + (tmpVectorDirection / 2) + Quaternion.Euler(0, 90, 0) * tmpVectorDirection))); 
                //tmpListPositions.Add((tmpPositionPivotPinza + (tmpVectorDirection / tmpTamanioSegmento) * i) + Quaternion.Euler(0, 90, 0) * tmpVectorDirection * Vector2.Distance(tmpPositionPivotPinza + (tmpVectorDirection / tmpTamanioSegmento) * i, VectorMoreNear(tmpPositionPivotPinza + (tmpVectorDirection / tmpTamanioSegmento) * i, tmpPositionPivotPinza, tmpPositionPivotPunta))); 
            }

            tmpListPositions.Add(new Vector3(tmpPositionPivotPunta[0], tmpPositionPivotPunta[1])); // ((x-h)^2  /  4p ) + gravityForEachSubdivision = y

            lineRenderCable.positionCount = tmpListPositions.Count;
            lineRenderCable.SetPositions(tmpListPositions.ToArray());
        }

        private Vector2 VectorMoreNear(Vector2 argVInit, Vector2 argV1Init, Vector2 argV2Init)
        {
            var tmpDistanceV1 = Vector2.Distance(argVInit, argV1Init);
            var tmpDistanceV2 = Vector2.Distance(argVInit, argV2Init);

            return (tmpDistanceV1 < tmpDistanceV2) ? argV1Init : argV2Init;
        }


        public static Rect RectTransformToScreenSpace(RectTransform transform)
        {
            Vector2 size = Vector2.Scale(transform.rect.size, transform.lossyScale);
            return new Rect((Vector2)transform.position - (size * 0.5f), size);
        }
    }
}