﻿using NSActiveZones;
using NSBoxMessage;
using NSInterfaz;
using NSInterfazAvanzada;
using NSPinzaVoltaje;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;
using NSTraduccionIdiomas;

namespace NSSituacion3
{
    public class ControladorSituacion4 : MonoBehaviour
    {
        #region members

        public GraficaPractica4 clsGraficadora;

        private bool estoyEnElTrafoPn = false;

        public GameObject[] ImgenesCortacircuitos;

        [SerializeField]
        private GameObject BotonPnCortacircuito;

        [SerializeField]
        private GameObject botonPnPinza;

        [SerializeField]
        private GameObject transformadorPn;

        private bool zonaSeguridadColocada;
       
        private bool avatarVestido;

        private bool implementosAlturaColocados;

        private bool ImplemntosSeñalizacion =false;

        [SerializeField] private ActiveZonesController refActiveZonesController;

       [SerializeField] private clsRegistroDatosSituacion4 refRegistroDatosSituacion4;

        [SerializeField] private ZonaActual zonaActual = ZonaActual.Afuera;

        [SerializeField, Header("Valores transformador")]
        private Transformador[] arrayTransformadores;

        [SerializeField] private Image imageTransformador;

      //[SerializeField] private clsTablet refTablet;

        private int indexTransformadorSeleccionado;

        private float[] corrienteCablesTransformador = new float[3];

        private float[] corrienteCablesMacromedidor = new float[3];

        private float[] tensionLineaPuntasPinza = new float[3];

        private float[] tensionFasePuntasPinza = new float[3];

        private float[] potenciaMacroMedidor = new float[3];

        /// <summary>
        /// ///pasra el poste pn----------------------------------------------------------------------------------------------------
        /// </summary>

        private float[] corrienteCablesTransformadorPn = new float[3];

        private float[] corrienteCablesMacromedidorPn = new float[3];

        private float[] tensionLineaPuntasPinzaPn = new float[3];

        private float[] tensionFasePuntasPinzaPN = new float[3];

        private float[] potenciaMacroMedidorPN = new float[3];

        /// fin para el poste pn--------------------------------------------------------------------------------------------------
        private float potenciaTransformadorActual;

        private float potenciaTransformadorActualPn;

        private int tramoSenializado;

        private int tramoDaniado;

        private bool senializacionColocada;

        [Header("Dependencias UI interfaz")]
        [SerializeField] private PanelInterfazSoloAnimacion refPanelInterfazSoloAnimacionVestimentaSeguridad;

        [SerializeField] private PanelInterfazSoloAnimacion refPanelInterfazSoloAnimacionSenializacionSeguridad;

        [SerializeField] private PanelInterfazSoloAnimacion refPanelInterfazSoloAnimacionHerramientasAlturas;

        [SerializeField] private CanvasGroup canvasGroupBotonesDerecha;

        [SerializeField] private CanvasGroup canvasGroupBotonesIzquierda;
        #endregion

        #region Herramientas

        [Header("Herramientas"), Space(10)]
        [Header("Descolgar corta circuitos"), Space(10)]
        [SerializeField] private GameObject pertigaDescolgarFusibles;

        [SerializeField] private GameObject pertigaMensajeNoOperar;

        [SerializeField] private GameObject letreroNoOperar;

        [SerializeField] private GameObject pertigaQuitarLetrero;

        [SerializeField] private GameObject pertigaColgarFusibles;

        private bool cortaCircuitosConectados;

        [Header("Puesta tierra"), Space(10)]
        [SerializeField] private GameObject pertigaPinzas;

        [SerializeField] private GameObject electrodoPuestaTierra;

        [SerializeField] private GameObject quitarPinzasZonaActiva;

        [Header("Herramientas recurrentes"), Space(10)]
        [SerializeField] private GameObject puntaDetectoraTension;

        [SerializeField] private GameObject pinzaAmperimetro;

        [SerializeField] private CheckCorrectTogglesActivate refCheckCorrectTogglesHerramientasAlturas;

        [SerializeField] private CheckCorrectTogglesActivate refCheckCorrectTogglesVestimentaSeguridad;

        [SerializeField] private CheckCorrectTogglesActivate refCheckCorrectToggleSenializacionSeguridad;

        [SerializeField] private CheckCorrectTogglesActivate refCheckCorrectToggleSeguridad;

        /// <summary>
        /// para saber en que etapa del corta circuito se va en el uso de herramientas
        /// </summary>
        private int etapaHerramientasCortaCircuitos;

        /// <summary>
        /// para saber en que etapa de la puesta a tierra se va, si en colocar el electrodo o las pinzas
        /// </summary>
        private int etapaHerramientasPuestaTierra;

        [SerializeField] private Button buttonVestirAvatar;

        [Header("Practica4 variables")]//--------------------------------------------------------------------------------------
        [SerializeField] private trasnsformadorPrac4[] Trasformadores;
        [SerializeField] private int IdtransformadorActual;
        private bool trasnformadorarmadoInstaldo;

        #endregion

        #region animaciones

        [Header("Animators"), Space(10)]
        [SerializeField] private Animator animatorCortaCircuitos;

        [Header("Animators controller"), Space(10)]
        [SerializeField] private RuntimeAnimatorController animatorControllerDescolgarCortaCircuitos;

        private int countTriggerAnimationDescolgarCortaCircuitos;

        [SerializeField] private RuntimeAnimatorController animatorControllerColgarCortaCircuitos;

        private int countTriggerAnimationColgarCortaCircuitos;
        #endregion

        #region zonas activas

        [Header("Zonas Activas")]
        [SerializeField] private GameObject zonasActivasDescolgarFusibles;

        [SerializeField] private GameObject zonasActivasColgarFusibles;

        [SerializeField] private GameObject zonaActivaLetreroNoOperar;

        [SerializeField] private GameObject zonaActivaQuitarLetreroNoOperar;

        #endregion

        #region cortaCircuitos
        /*
        [Header("Senializacion tramos")]
        [SerializeField]
        private GameObject[] arrayTramosSenializacion;

        [Header("Cambio cables tramos")]
        [SerializeField]
        private GameObject[] arrayTramosCambioCables;
        */
        #endregion

        #region accessors
        public bool _ImplemntosSeñalizacion
        {
            get { return ImplemntosSeñalizacion; }
        }
        public bool _implementosAlturaColocados
        {
            get { return implementosAlturaColocados; }
        }

        public int _indexTransformadorSeleccionado
        {
            get { return indexTransformadorSeleccionado; }
        }

        public bool _trasnformadorarmadoInstaldo
        {
            set {
                trasnformadorarmadoInstaldo = value;
               
            }
            get { return trasnformadorarmadoInstaldo; }
        }

        public bool _zonaSeguridadColocada
        {
            set
            {
                zonaSeguridadColocada = value;
                var tmpPreparadoSituacion = zonaSeguridadColocada && avatarVestido;
                SetEtapaZonaSeguridad();
            }
            get { return zonaSeguridadColocada; }
        }

        public bool _avatarVestido
        {
            set
            {
                avatarVestido = value;
                var tmpPreparadoSituacion = zonaSeguridadColocada && avatarVestido;
                SetEtapaVestirAvatar();
            }
            get { return avatarVestido; }
        }

        public float[] _corrienteCablesTransformador
        {
            get {
                if (estoyEnElTrafoPn)
                {
                    return corrienteCablesTransformadorPn;
                }
                else
                {
                    return corrienteCablesTransformador;
                }
            }
        }

        public float[] _corrienteCablesMacromedidor
        {
            get
            {
                if (estoyEnElTrafoPn)
                {
                    return corrienteCablesMacromedidorPn;
                }
                else
                {
                    return corrienteCablesMacromedidor;
                }
            }
        }

        public float[] _tensionLineaPuntasPinza
        {
            get
            {
                if (estoyEnElTrafoPn)
                {
                    return tensionLineaPuntasPinzaPn;
                }
                else
                {
                    return tensionLineaPuntasPinza;
                }
            }
        }

        public float[] _tensionFasePuntasPinza
        {
            get
            {
                if (estoyEnElTrafoPn)
                {
                    return tensionFasePuntasPinzaPN;
                }
                else
                {
                    return tensionFasePuntasPinza;
                }
            }
        }


        public ZonaActual _etapaSituacionActual
        {
            get { return zonaActual; }
        }

        public int _etapaHerramientasPuestaTierra
        {
            get
            {
                return etapaHerramientasPuestaTierra;
            }
            set
            {
                etapaHerramientasPuestaTierra = value;
            }
        }

        public float _potenciaTransformadorActual
        {
            get { return potenciaTransformadorActual; }
        }

        public int _etapaHerramientasCortaCircuitos
        {
            set
            {
                etapaHerramientasCortaCircuitos = value;
            }
        }
        #endregion

        private void Awake()
        {
            //refCortaCircuitoDesconectadoRamdom.SetRamdomCanuelaDesconectada();
           // refTablet.IniciarlizarValores();
            SetTransformadorRamdom();

            tramoDaniado = Random.Range(0,4);

            refActiveZonesController.InteractuableZone("TransformadorPertiga", false);
            refActiveZonesController.InteractuableZone("MedicionTension", false);
            refActiveZonesController.InteractuableZone("TransformadorPinza", false);
            refActiveZonesController.InteractuableZone("ZonasSeguridad", false);
            refActiveZonesController.InteractuableZone("ZonasSeguridadInicial", false);
            refActiveZonesController.InteractuableZone("CambiarCable", false);

        }
        private void Start()
        {   InitTrasforamdor();
            CalcularValoresSistema();
        }

        #region private methods


        private void OnButtonAceptarMensajeInformacion()
        {
            refActiveZonesController.ActiveAllZones();
        }

        private void SetTransformadorRamdom()
        {
            indexTransformadorSeleccionado = Random.Range(0, arrayTransformadores.Length);
            imageTransformador.sprite = arrayTransformadores[indexTransformadorSeleccionado].spriteTransformador;
        }

        /*
        private void ActivarPanelTramoCambioCables(int argIndexTramo)
        {
            for (int i = 0; i < arrayTramosCambioCables.Length; i++)
                arrayTramosCambioCables[i].SetActive(i == argIndexTramo);
        }*/

        /// <summary>
        /// Calcula todos los valores del sistema al azar, en el awake de este script y luego en el evento de cuando se cambian todos los dps
        /// </summary>
        public void CalcularValoresSistema()
        {
            Debug.Log("estamso en rcolectando los datos");

            potenciaTransformadorActual = (Trasformadores[IdtransformadorActual].potenciaKVA * 1000f);

            for (int i = 0; i < 3; i++)
            {
                corrienteCablesTransformador[i] = (potenciaTransformadorActual / 360.26656f) * (Random.Range(0.975f, 1.025f) + 0.01f);
                corrienteCablesMacromedidor[i] = corrienteCablesTransformador[i] / 40; //gran variacion del 0.02%
                tensionLineaPuntasPinza[i] = potenciaTransformadorActual / (1.732050f * corrienteCablesTransformador[i]);
                tensionFasePuntasPinza[i] = tensionLineaPuntasPinza[i] / 1.732050f;
                potenciaMacroMedidor[i] = 1.732050f * tensionLineaPuntasPinza[i] * corrienteCablesMacromedidor[i] * 0.95f;
            }

            potenciaTransformadorActualPn = (Trasformadores[retornaElIdDeltransforamdorELejido()].potenciaKVA * 1000f);
            for (int i = 0; i < 3; i++)
            {
                corrienteCablesTransformadorPn[i] = (potenciaTransformadorActualPn / 360.26656f) * (Random.Range(0.975f, 1.025f) + 0.01f);
                corrienteCablesMacromedidorPn[i] = corrienteCablesTransformadorPn[i] / 40; //gran variacion del 0.02%
                tensionLineaPuntasPinzaPn[i] = potenciaTransformadorActualPn / (1.732050f * corrienteCablesTransformadorPn[i]);
                tensionFasePuntasPinzaPN[i] = tensionLineaPuntasPinzaPn[i] / 1.732050f;
                potenciaMacroMedidorPN[i] = 1.732050f * tensionLineaPuntasPinzaPn[i] * corrienteCablesMacromedidorPn[i] * 0.95f;
            }
            refRegistroDatosSituacion4.SetDatosTransformadorPN(tensionFasePuntasPinzaPN[0], tensionFasePuntasPinzaPN[1], tensionFasePuntasPinzaPN[2], Trasformadores[IdtransformadorActual].valorCorretoFusible);

        }


        #endregion

        #region public methods

        public void calificarTrafoYFusible()
        {
            refRegistroDatosSituacion4.verificarTransformadoryFusible();
        }


        public void OnButtonZonaActivaTransformador()
        {
            Debug.Log("OnButtonZonaActivaTransformador");

            if (avatarVestido)
            {
                if (ImplemntosSeñalizacion)
                {
                    PanelInterfazTransformador._instance.Mostrar();
                    SetEtapaTransformador();
                }
                else
                {
                    refActiveZonesController.DesactiveAllZones();
                    BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("TextElementosDeSeñalizacion"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"), OnButtonAceptarMensajeInformacion);
                }

            }
            else
            {
                refActiveZonesController.DesactiveAllZones();
                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("TextElementosDeSeguridad"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"), OnButtonAceptarMensajeInformacion);
            }
        }

        public void OnButtonZonaActivaTransformadorPertiga()
        {
            refActiveZonesController.InteractuableZone("TransformadorPertiga", false);
            refActiveZonesController.DesactivateZone("TransformadorPertiga");

            if (etapaHerramientasCortaCircuitos == 0)
            {

                pertigaColgarFusibles.SetActive(true);
                zonasActivasDescolgarFusibles.SetActive(true);
                countTriggerAnimationColgarCortaCircuitos = 0;
                countTriggerAnimationDescolgarCortaCircuitos = 0;


                for (int i = 0; i < zonasActivasColgarFusibles.transform.childCount; i++)
                    zonasActivasColgarFusibles.transform.GetChild(i).gameObject.SetActive(true);
            }

            PanelInterfazCortaCircuitos._instance.Mostrar();
        }

        public void OnButtonZonaActivaTransformadorPuntaTension()
        {
            puntaDetectoraTension.SetActive(true);
            refActiveZonesController.InteractuableZone("MedicionTension", false);
            refActiveZonesController.DesactivateZone("MedicionTension");
            PanelInterfazTransformador._instance.Mostrar(true);
        }

        public void OnButtonZonaActivaTransformadorPinza()
        {
            pinzaAmperimetro.SetActive(true);
            pinzaAmperimetro.GetComponent<PinzaVoltaje>().RotarPinza(0);
            refActiveZonesController.InteractuableZone("TransformadorPinza", false);
            refActiveZonesController.DesactivateZone("TransformadorPinza");
            PanelInterfazTransformador._instance.Mostrar();
            if (trasnformadorarmadoInstaldo) {
                botonPnPinza.SetActive(false);
                transformadorPn.SetActive(true);
            }
        }

        public void OnButtonZonaActivaPuestaTierra()
        {
            if (cortaCircuitosConectados)
            {
                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("textoCortaCircuitos"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"), OnButtonAceptarMensajeInformacion);
                return;
            }

            refActiveZonesController.InteractuableZone("PuestaTierra", false);
            refActiveZonesController.DesactivateZone("PuestaTierra");
            PanelInterfazPuestaTierra._instance.Mostrar(true);
        }

        /// <summary>
        /// Selecciona el tramo con el indice indicado para medir tension
        /// </summary>
        /// <param name="argIndexTramo">Indice del tramo seleccionado</param>
        public void OnButtonZonaActivaTramoMedicionTension(int argIndexTramo)
        {
            PanelInterfazCables._instance.Mostrar(true);
            puntaDetectoraTension.SetActive(true);

            if (cortaCircuitosConectados)
                PanelInterfazCables._instance.SetTensionCables(true, true, true, true);
            else
                PanelInterfazCables._instance.SetTensionCables(false, false, false, false);

            refActiveZonesController.InteractuableZone("MedicionTension", false);
            refActiveZonesController.DesactivateZone("MedicionTension");
        }

        /// <summary>
        /// señaliza el tramo con el indice indicado
        /// </summary>
        /// <param name="argIndexTramo"></param>
        public void OnButtonZonaActivaTramoSenializacionSeguridad(int argIndexTramo)
        {
            tramoSenializado = argIndexTramo;
            ActivarTramoSenializacion(argIndexTramo);
            refActiveZonesController.DesactivateZone("ZonasSeguridad");
            refActiveZonesController.InteractuableZone("ZonasSeguridad", false);
            canvasGroupBotonesDerecha.interactable = true;
            canvasGroupBotonesIzquierda.interactable = true;
        }

        /// <summary>
        /// señaliza el tramo con el indice indicado
        /// </summary>
        /// <param name="argIndexTramo"></param>
        public void OnButtonZonaActivaTramoPinza(int argIndexTramo)
        {
            PanelInterfazCables._instance.Mostrar(true);
            pinzaAmperimetro.SetActive(true);
            pinzaAmperimetro.GetComponent<PinzaVoltaje>().RotarPinza(90f);
            //ActivarTramoSenializacion(argIndexTramo);
            refActiveZonesController.DesactivateZone("TransformadorPinza");
            refActiveZonesController.InteractuableZone("TransformadorPinza", false);
        }

        /// <summary>
        /// muestra los cables del tramo seleccionado
        /// </summary>
        /// <param name="argIndexTramo"></param>
        public void OnButtonZonaActivaTramoCables(int argIndexTramo)
        {
            PanelInterfazCables._instance.Mostrar(true);
            //ActivarTramoSenializacion(argIndexTramo);
            refActiveZonesController.DesactivateZone("CambiarCable");
            refActiveZonesController.InteractuableZone("CambiarCable", false);
            ///ActivarPanelTramoCambioCables(argIndexTramo);-----------------------------------------------------------------------

        }

        public void OnButtonZonaActivaCortacircuito()
        {
            Debug.Log("OnButtonZonaActivaCortacircuito");

            if (avatarVestido)
            {
                if (!zonaSeguridadColocada)
                {
                    //refControladorInstructor.MostrarMensajeInstructor("No puede realizar ningún trabajo hasta que señalice el área de trabajo correctamente.");
                    //BoxMessageManager._instance.MtdCreateBoxMessageInfo("No se ha colocado la señalización correcta para realizar la maniobra.", "ACEPTAR", OnButtonAceptarMensajeInformacion);
                    return;
                }

                refActiveZonesController.DesactiveAllZones();
                PanelInterfazCortaCircuitos._instance.Mostrar();
                SetEtapaCortaCircutosMalo();
            }
            else
            {
                refActiveZonesController.DesactiveAllZones();
                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("textNoseaPreparado"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"), OnButtonAceptarMensajeInformacion);
            }
        }

        public void AgregarMensajesEnviados()
        {
            //mensajesEnviadosCentroControl++;
        }

        public void OnButtonValidarVestimentaSeguridad()
        {
            if (refCheckCorrectTogglesVestimentaSeguridad.VerifyGroupToggles("VestimentaSeguridad"))
            {
                avatarVestido = true;
                refPanelInterfazSoloAnimacionVestimentaSeguridad.Mostrar(false);
                //buttonVestirAvatar.interactable = false;
                Debug.Log("OnButtonValidarVestimentaSeguridad");
            }
            else
                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("TextProtecionNoAdecuada"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));

        }

        public void OnButtonToggleCono()
        {
            /*if (senializacionColocada)
                return;*/

            if (avatarVestido)
            {
                refCheckCorrectToggleSenializacionSeguridad.DesactivateAllToggles();
                refPanelInterfazSoloAnimacionSenializacionSeguridad.Mostrar(false);
                refActiveZonesController.InteractuableZone("ZonasSeguridadInicial", true);
                refActiveZonesController.ActiveZone("ZonasSeguridadInicial");
                canvasGroupBotonesDerecha.interactable = false;
                canvasGroupBotonesIzquierda.interactable = false;
                senializacionColocada = true;
            }
            else
                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("TextElementosDeSeguridad"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
        }

        public void OnButtonValidarHerramienta()
        {
            DesactivarHerramientas();

            switch (zonaActual)
            {
                case ZonaActual.Null:
                    BoxMessageManager._instance.MtdCreateBoxMessageInfo("En este escenario no se pueden utilizar las herramientas disponibles.", DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"), OnButtonAceptarMensajeInformacion);
                    PanelInterfazCajaHerramientas._instance.Mostrar(false);
                    break;

                case ZonaActual.VestirAvatar:
                    BoxMessageManager._instance.MtdCreateBoxMessageInfo("En este escenario no se pueden utilizar las herramientas disponibles.", DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"), OnButtonAceptarMensajeInformacion);
                    PanelInterfazCajaHerramientas._instance.Mostrar(false);
                    break;

                case ZonaActual.ZonaSeguridad:

                    break;

                case ZonaActual.Afuera:

                    //check normal
                    if (refCheckCorrectTogglesHerramientasAlturas.VerifyGroupToggles("PertigaAfuera"))
                    {
                        if (trasnformadorarmadoInstaldo)
                        {
                            //refActiveZonesController.InteractuableZone("TransformadorPertiga", true);
                            //refActiveZonesController.ActiveZone("TransformadorPertiga");
                            //refActiveZonesController.ActiveZone("ZonaInicalPosteP0", false); 
                            refActiveZonesController.ActiveZone("ZonaInicalPostePn", true);
                            refActiveZonesController.InteractuableZone("ZonaInicalPostePn", true);
                            refPanelInterfazSoloAnimacionHerramientasAlturas.Mostrar(false);
                            BotonPnCortacircuito.SetActive(true);
                            refActiveZonesController.InteractuableZone("ZonaInicalPosteP0", false);
                            Debug.Log("CotacircuitosTrafoPn activo");
                            return;
                        }
                        else
                        {
                            BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("textHerramientasSinFunsion"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                            return;
                        }
                    }
                    else if (refCheckCorrectTogglesHerramientasAlturas.VerifyGroupToggles("PuntaMedidoraTension"))
                    {
                        refActiveZonesController.InteractuableZone("MedicionTension", true);
                        refActiveZonesController.ActiveSpecificZone("MedicionTension", new int[] { 0, tramoSenializado + 1 });
                        refPanelInterfazSoloAnimacionHerramientasAlturas.Mostrar(false);
                        return;
                    }
                    else if (refCheckCorrectTogglesHerramientasAlturas.VerifyGroupToggles("pinzaMedidora"))
                    {
                        if (!implementosAlturaColocados)
                        {
                            BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("TextElementosDeSeguridad"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                            return;
                        }
                        if (!ImplemntosSeñalizacion)
                        {
                            BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("TextElementosDeSeñalizacion"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                            return;
                        }
                        refPanelInterfazSoloAnimacionHerramientasAlturas.Mostrar(false);
                        refActiveZonesController.InteractuableZone("TransformadorPinza", true);
                        refActiveZonesController.ActiveSpecificZone("TransformadorPinza", new int[] {0});
                        refActiveZonesController.InteractuableZone("ZonaInicalPosteP0", false);
                        refActiveZonesController.InteractuableZone("ZonaInicalPostePn", false);

                        if (trasnformadorarmadoInstaldo&& cortaCircuitosConectados) {
                            refActiveZonesController.InteractuableZone("ZonaInicalPostePn", true);
                            botonPnPinza.SetActive(true);
                            transformadorPn.SetActive(false);
                            Debug.Log("PinzaTrafoPn activo");
                        }

                        return;
                    }

                    else if (trasnformadorarmadoInstaldo)
                    {
                        Debug.Log("el transformdor esta on y se va a colgar lso corta circuitos");

                        if (refCheckCorrectTogglesHerramientasAlturas.VerifyGroupToggles("ColgarCortaCircuitos"))
                        {
                            animatorCortaCircuitos.enabled = false;
                            zonasActivasColgarFusibles.SetActive(true);
                            pertigaColgarFusibles.SetActive(true);
                            refPanelInterfazSoloAnimacionHerramientasAlturas.Mostrar(false);
                        }
                    }

                    BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("textHerramientasSinFunsion"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                    break;
                

                case ZonaActual.CortaCircuitos:

                    if (trasnformadorarmadoInstaldo)
                    {
                        Debug.Log("el Tranfoamdor esta on y se va a colgar los corta circuitos");

                        if (refCheckCorrectTogglesHerramientasAlturas.VerifyGroupToggles("ColgarCortaCircuitos"))
                        {
                            animatorCortaCircuitos.enabled = false;
                            zonasActivasColgarFusibles.SetActive(true);
                            pertigaColgarFusibles.SetActive(true);
                            refPanelInterfazSoloAnimacionHerramientasAlturas.Mostrar(false);
                            refActiveZonesController.ActiveZone("CotacircuitosTrafoPn");
                            Debug.Log("CotacircuitosTrafoPn activo");
                        }
                        else
                            BoxMessageManager._instance.MtdCreateBoxMessageInfo("Debe colgar de nuevo los corta circuitos", DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                    }
                       break;

                case ZonaActual.Transformador:

                    if (refCheckCorrectTogglesHerramientasAlturas.VerifyGroupToggles("PuntaMedidoraTension"))
                    {
                        DesactivarHerramientas();
                        puntaDetectoraTension.SetActive(true);
                        refPanelInterfazSoloAnimacionHerramientasAlturas.Mostrar(false);
                    }
                    else if (refCheckCorrectTogglesHerramientasAlturas.VerifyGroupToggles("PinzaAmperimetrica"))
                    {
                        pinzaAmperimetro.SetActive(true);
                        refPanelInterfazSoloAnimacionHerramientasAlturas.Mostrar(false);
                    }
                    else if (refCheckCorrectTogglesHerramientasAlturas.AnyToggleActive())
                        BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeHerramientaInadecuada"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                    else
                        BoxMessageManager._instance.MtdCreateBoxMessageInfo("No ha seleccionadoninguna herramienta.", DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));

                    break;

            }

            refCheckCorrectTogglesHerramientasAlturas.DesactivateAllToggles();
        }

        public void OnButtonValidarImplementosAlturas()
        {

            if (refCheckCorrectToggleSenializacionSeguridad.VerifyGroupToggles("ImplementosTrabajoAlturas"))
            {
                refPanelInterfazSoloAnimacionSenializacionSeguridad.Mostrar(false);
                implementosAlturaColocados = true;
                if (refCheckCorrectToggleSeguridad.VerifyGroupToggles("ImplementosSeñalizacion"))
                {
                    refPanelInterfazSoloAnimacionSenializacionSeguridad.Mostrar(false);
                    ImplemntosSeñalizacion = true;
                }
            }
            else
            {
                implementosAlturaColocados = false; ;
                if (refCheckCorrectToggleSeguridad.VerifyGroupToggles("ImplementosSeñalizacion"))
                {
                    refPanelInterfazSoloAnimacionSenializacionSeguridad.Mostrar(false);
                    ImplemntosSeñalizacion = true;
                }
                else
                {
                    ImplemntosSeñalizacion = false;
                    BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("textImplementosSeleccionados"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                }
            }
        }

        public void DesactivarHerramientas()
        {
            puntaDetectoraTension.SetActive(false);
            pinzaAmperimetro.SetActive(false);
            pertigaColgarFusibles.SetActive(false);
            pertigaDescolgarFusibles.SetActive(false);
            botonPnPinza.SetActive(false);
            BotonPnCortacircuito.SetActive(false);
            refActiveZonesController.OnActiveAllZones.Invoke();
            refActiveZonesController.InteractuableZone("ZonaInicalPosteP0", true);
            refActiveZonesController.InteractuableZone("ZonaInicalPostePn", true);
            zonaActual = ZonaActual.Afuera;

        }

        //get values

       /* public float GetCorrienteCablesTransformadorSituacion3(int argIndex)
        {
           // return refTablet.GetCorrienteDeCablesPorTramo(tramoSenializado)[argIndex];
        }

        public float GetTensionLineaPuntasPinzaSituacion3(int argIndex)
        {
            //return refTablet.tensioLineaALinea(tramoSenializado)[argIndex];
        }

        public float GetTensionFasePuntasPinzaSituacion3(int argIndex)
        {
            //return refTablet.tensioLineaAFase(tramoSenializado)[argIndex];
        }
        */

        //set Zones
        public void SetEtapaVestirAvatar()
        {
            Debug.Log("SetEtapaVestirAvatar");
            zonaActual = ZonaActual.VestirAvatar;
        }

        public void SetEtapaZonaSeguridad()
        {
            Debug.Log("SetEtapaZonaSeguridad");
            zonaActual = ZonaActual.ZonaSeguridad;
        }
        

        public void SetEtapaVisualizacionCortaCircuito()
        {
            Debug.Log("SetEtapaVisualizacionCortaCircuito");
            zonaActual = ZonaActual.CortaCircuitos;
            puntaDetectoraTension.SetActive(false);
            pinzaAmperimetro.SetActive(false);
        }

        public void SetEtapaCortaCircutosMalo()
        {
            Debug.Log("SetEtapaCortaCircuto");
            zonaActual = ZonaActual.CortaCircuitos;
            puntaDetectoraTension.SetActive(false);
            pinzaAmperimetro.SetActive(false);
        }

        public void SetEtapaTransformador()
        {
            Debug.Log("SetEtapaTransformador");
            zonaActual = ZonaActual.Transformador;
        }

        public void SetEtapaPuestaTierra()
        {
            Debug.Log("SetEtapaPuestaTierra");
            zonaActual = ZonaActual.PuestaTierra;
        }

        public void SetEtapaAfuera()
        {
            Debug.Log("SetEtapaAfuera");
            zonaActual = ZonaActual.Afuera;
        }

        public void EjecutarAnimacionDescolgarCortaFusibles()
        {
            countTriggerAnimationDescolgarCortaCircuitos++;

            if (countTriggerAnimationDescolgarCortaCircuitos == 3)
            {
                etapaHerramientasCortaCircuitos = 1;
                cortaCircuitosConectados = false;
                animatorCortaCircuitos.enabled = true;
                animatorCortaCircuitos.runtimeAnimatorController = animatorControllerDescolgarCortaCircuitos;
                animatorCortaCircuitos.SetBool("Descolgar", true);
                pertigaDescolgarFusibles.SetActive(false);
                zonasActivasDescolgarFusibles.SetActive(false);
                pertigaDescolgarFusibles.SetActive(false);
                zonaActual = ZonaActual.CortaCircuitos;
                StartCoroutine(CouDesactivarAnimatorDescolgarFusibles());
        
            }
        }

        public void EjecutarAnimacionColgarCortaFusibles()
        {
            countTriggerAnimationColgarCortaCircuitos++;

            if (countTriggerAnimationColgarCortaCircuitos == 3)
            {
                cortaCircuitosConectados = true;
                animatorCortaCircuitos.enabled = true;
                animatorCortaCircuitos.runtimeAnimatorController = animatorControllerColgarCortaCircuitos;
                animatorCortaCircuitos.SetBool("Colgar", true);
                pertigaColgarFusibles.SetActive(false);
                zonasActivasColgarFusibles.SetActive(false);
                BotonPnCortacircuito.SetActive(false);

                StartCoroutine(CouDesactivarAnimatorColgarFusibles());
                refActiveZonesController.InteractuableZone("ZonaInicalPosteP0", true);
                refActiveZonesController.InteractuableZone("ZonaInicalPostePn", true);
                transformadorPn.SetActive(true);
                ImgenesCortacircuitos[0].SetActive(false);
                ImgenesCortacircuitos[1].SetActive(true);
                graficarNuevaTrafoPnIntalado();

            }
        }

        public void SetLetreroNoOperarCortaCircuitos()
        {
            Debug.Log("SetLetreroNoOperarCortaCircuitos");
            pertigaMensajeNoOperar.SetActive(false);
            letreroNoOperar.SetActive(true);
            etapaHerramientasCortaCircuitos = 2;
            //zonaActual = ZonaActual.DPSs;
        }

        public void DesactivarTodaTension()
        {
            /*refControladorTensionCortaCircuitos.DesactivarTensionInferior();
            refControladorTensionDPS.DesactivarTension();
            refControladorTensionTransformador.DesactivarTension();
            */
        }
        //----------------------------------------------------------------------------------practica 4--------------------------------------------------
        public void MostrarImagenTrafoP0() {
            if (trasnformadorarmadoInstaldo)
            {   desactivarImgenesTrafo();
                Trasformadores[IdtransformadorActual].ObjTransformador.SetActive(true);
                estoyEnElTrafoPn = false;
            }
        }

        public void MostrarImagenTrafoPn()
        {
            if (trasnformadorarmadoInstaldo)
            {
                desactivarImgenesTrafo();
                var id = retornaElIdDeltransforamdorELejido();
                Trasformadores[id].ObjTransformador.SetActive(true);
                estoyEnElTrafoPn = true;
            }
        }


        private void ActivarTramoSenializacion(int argIndexTramo)
        {
           /* for (int i = 0; i < arrayTramosSenializacion.Length; i++)
                arrayTramosSenializacion[i].SetActive(i == argIndexTramo);
                ---------------------------------------------------*/
        }

        #endregion

        #region courutines

        private IEnumerator CouDesactivarAnimatorDescolgarFusibles()
        {
            yield return new WaitForSeconds(3);
            animatorCortaCircuitos.enabled = false;
            etapaHerramientasCortaCircuitos = 1;
        }

        private IEnumerator CouDesactivarAnimatorColgarFusibles()
        {
            yield return new WaitForSeconds(3.25f);
            animatorCortaCircuitos.enabled = false;
            etapaHerramientasCortaCircuitos = 3;
        }

        //-------------------------------------------------------------------------------practica 4---------------------------------------------------------------
        
        private void InitTrasforamdor()
        {
            for (int i=0;i< Trasformadores.Length;i++)
                Trasformadores[i].ObjTransformador.SetActive(false);

            IdtransformadorActual = Random.Range(0, 4);
            Trasformadores[IdtransformadorActual].ObjTransformador.SetActive(true);
            Trasformadores[IdtransformadorActual].esCalaDenumeros.SetActive(true);
            graficarPosteSeleccionado();
            refRegistroDatosSituacion4.SetDatosMedidasPotencias(Trasformadores[IdtransformadorActual].PotensiaDelSIstema, Trasformadores[IdtransformadorActual].valorCorrectotranforadorPn, Trasformadores[IdtransformadorActual].PotensiaDelSIstema -Trasformadores[IdtransformadorActual].potenciaKVA);
        }


        private void graficarPosteSeleccionado()
        {
            Debug.Log("pintar grafica");
            var CordenadasConvertidas = clsGraficadora.ConvertirVectorAcordenadasDeLagrafica(Trasformadores[IdtransformadorActual].DatosDeLaGrafica, Trasformadores[IdtransformadorActual].valorMAximoParalagrafica);

            var vectorX = clsGraficadora.ValoresParaX(Trasformadores[IdtransformadorActual].DatosDeLaGrafica.Length);

            Vector3[] aux = new Vector3[Trasformadores[IdtransformadorActual].DatosDeLaGrafica.Length];
            
            for (int i = 0; i < CordenadasConvertidas.Length; i++)
            {

                aux[i] = new Vector3( vectorX[i], CordenadasConvertidas[i], 0f);
            }

            clsGraficadora.GenerarGrafica(aux);
        }

        /// <summary>
        /// grafica que muestra la slucion del sistema segun el trafo elejido para pn"
        /// </summary>
        private void graficarNuevaTrafoPnIntalado()
        {
            var potenciaTrafoPn= Trasformadores[retornaElIdDeltransforamdorELejido()].potenciaKVA;
            var CordenadasConvertidas = clsGraficadora.ConvertirVectorSolucionNuevotrafo(Trasformadores[IdtransformadorActual].DatosDeLaGrafica, Trasformadores[IdtransformadorActual].valorMAximoParalagrafica, Trasformadores[IdtransformadorActual].porcentajesPortrafoElegido[retornaElIdDeltransforamdorELejido()]);
            var vectorX = clsGraficadora.ValoresParaX(Trasformadores[IdtransformadorActual].DatosDeLaGrafica.Length);

            Vector3[] aux = new Vector3[Trasformadores[IdtransformadorActual].DatosDeLaGrafica.Length];

            for (int i = 0; i < CordenadasConvertidas.Length; i++)
            {
                aux[i] = new Vector3(vectorX[i], CordenadasConvertidas[i], 0f);
            }

            clsGraficadora.GenerarGrafica(aux);

        }

        private void desactivarImgenesTrafo()
        {
            for (int i=0; i < Trasformadores.Length;i++) 
                Trasformadores[i].ObjTransformador.SetActive(false);
        }


        private int retornaElIdDeltransforamdorELejido()
        {
            int aux=0;
            switch (refRegistroDatosSituacion4.inputTensionTransformador[1].text)
            {
                case "45":
                    aux = 4;
                    break;              
                case "75":
                    aux =0;
                    break;
                case "112.5":
                    aux = 1;
                    break;
                case "150":
                    aux = 2;
                    break;
                case "225":
                    aux = 3;
                    break;
            }
            return aux;
        }
        #endregion
        
    }

    [Serializable]
    public class trasnsformadorPrac4
    {
        public float potenciaKVA;

        public GameObject ObjTransformador;

        public float[] DatosDeLaGrafica;

        public float PotensiaDelSIstema;

        public float valorMAximoParalagrafica;

        public float valorCorrectotranforadorPn;

        public float valorCorretoFusible;

        public float[] porcentajesPortrafoElegido;

        public GameObject esCalaDenumeros;

    }

}