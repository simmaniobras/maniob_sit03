using System;
using UnityEngine;

namespace NSUtilities
{
    public class GameObjectSyncActive : MonoBehaviour
    {
        #region members

        [SerializeField] private GameObject gameObjectObjetive;
        
        #endregion

        #region mono behaviour

        private void OnEnable()
        {
            gameObjectObjetive.SetActive(true);
        }

        private void OnDisable()
        {
            gameObjectObjetive.SetActive(false);
        }
        #endregion
    }
}