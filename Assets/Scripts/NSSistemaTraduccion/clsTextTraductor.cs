﻿using System;
using NSTraduccionIdiomas;
using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Clase que se adjunta a cualquier text de la interfaz para que este se pueda traducir
/// </summary>
public class clsTextTraductor : MonoBehaviour
{
    #region monobehaviour

    private void Start()
    {
        DiccionarioIdiomas._instance.DLTraducirForNameObj += TraducirGameObjetText;
    }

    private void OnDestroy()
    {
        DiccionarioIdiomas._instance.DLTraducirForNameObj -= TraducirGameObjetText;
    }

    private void OnEnable()
    {
        TraducirGameObjetText();
    }
    #endregion

    #region private methods

    /// <summary>
    /// identifica el tipo de texto y lo traduse
    /// </summary>
    private void TraducirGameObjetText()
    {
        try
        {
            var tmpTextMeshProUGUI = GetComponent<TextMeshProUGUI>();

            if (tmpTextMeshProUGUI)
            {
                var traduccion = DiccionarioIdiomas._instance.Traducir(name);
                var textoConFormato = new StringBuilder(traduccion);
                tmpTextMeshProUGUI.SetText(textoConFormato);
                LayoutRebuilder.ForceRebuildLayoutImmediate(GetComponent<RectTransform>());
                LayoutRebuilder.ForceRebuildLayoutImmediate(transform.parent.GetComponent<RectTransform>());
                return;
            }
        }
        catch (Exception e)
        {
            Debug.LogError("Key no existe en el diccionario : "+ e);
        }
        
        try
        {
            var tmpTextDraw = GetComponent<TEXDraw>();

            if (tmpTextDraw)
            {
                var traduccion = DiccionarioIdiomas._instance.Traducir(name);
                var textoConFormato = new StringBuilder(traduccion);
                tmpTextDraw.text = textoConFormato.ToString();
                LayoutRebuilder.ForceRebuildLayoutImmediate(GetComponent<RectTransform>());
                LayoutRebuilder.ForceRebuildLayoutImmediate(transform.parent.GetComponent<RectTransform>());
            }
        }
        catch (Exception e)
        {
            Debug.LogError("Key no existe en el diccionario : "+ e);
        }
        
        /*var tmpText = GetComponent<Text>();

        if (tmpText)
        {
            tmpText.text = DiccionarioIdiomas._instance.Traducir(name, tmpText.text);
            return;
        }*/

        /*var tmpTextParaTraducir = GetComponent<TextParaTraducir>();

        if (tmpTextParaTraducir)
        {
            var traduccion = DiccionarioIdiomas._instance.Traducir(name, tmpTextParaTraducir.text);
            tmpTextParaTraducir.text = traduccion;
            return;
        }*/
    }
    #endregion
}