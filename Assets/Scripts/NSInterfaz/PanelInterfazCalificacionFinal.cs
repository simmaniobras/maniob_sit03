﻿using NSEvaluacion;
using NSInterfaz;
using NSSeguridad;
using NSTraduccionIdiomas;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NSInterfazAvanzada
{
    public class PanelInterfazCalificacionFinal : AbstractSingletonPanelUIAnimation<PanelInterfazCalificacionFinal>
    {
        #region members

        [SerializeField]
        private TextMeshProUGUI textValorCalificacion;

        [SerializeField]
        private TextMeshProUGUI textNombreUsuario;

        [SerializeField]
        private Evaluacion refEvaluacion;

        [SerializeField]
        private clsEnvioPdf refEnvioPdf;

        [SerializeField, Tooltip("Frases de aliento")]
        private TextMeshProUGUI textFelicitacion1;

        [SerializeField]
        private TextMeshProUGUI textFelicitacion2;

        [SerializeField]
        private string[] felicitacionMas60Porciento;

        [SerializeField]
        private string[] felicitacionMenos60Porciento;

        [SerializeField]
        private int practica;

        #endregion

        #region monoBehaviour

        private void OnEnable()
        {
            SetTextValorCalificacion(refEvaluacion.GetCalificacionTotalRango());
            float calificacionTotal=0;
            
            switch (practica)
            {
                case 1:
                    calificacionTotal = refEvaluacion._refCalificacionSituacion.GetCalificacionTotal();
                    break;
                case 2:
                    calificacionTotal = refEvaluacion._refCalificacionSituacion2.GetCalificacionTotal();
                    break;
                case 3:
                    calificacionTotal = refEvaluacion._refCalificacionSituacion3.GetCalificacionTotal();
                    break;
                case 4:
                    calificacionTotal = refEvaluacion._refCalificacionSituacion4.GetCalificacionTotal();
                    break;
            }

            //Asignar textos calificacion
            if (calificacionTotal > 0.6f)
            {
                textFelicitacion1.text = felicitacionMas60Porciento[0];
                textFelicitacion2.text = felicitacionMas60Porciento[1];
            }
            else
            {
                textFelicitacion1.text = felicitacionMenos60Porciento[0];
                textFelicitacion2.text = felicitacionMenos60Porciento[1];
            }

            var tmpDatosSesion = ClsSeguridad._instance.GetDatosSesion();
            SetTextNombreUsuario(tmpDatosSesion[0]);
        }
        #endregion

        #region private methods

        private void SetTextValorCalificacion(string argValorCalificacion)
        {
            Debug.Log("argValorCalificacion : " + argValorCalificacion);

            textValorCalificacion.text = argValorCalificacion;
        }

        private void SetTextNombreUsuario(string argNombreUsuario)
        {
            Debug.Log("argNombreUsuario : " + argNombreUsuario);
            textNombreUsuario.text = argNombreUsuario;
        }
        #endregion

        #region public methods

        public void OnButtonAceptar()
        {
            Mostrar(false);
            refEnvioPdf.VisualizarPDF();
        }
        #endregion
    }
}