﻿using System.Collections;
using System.Collections.Generic;
using NSActiveZones;
using NSBoxMessage;
using NSSituacion2;
using NSSituacion3;
using UnityEngine;
using UnityEngine.SceneManagement;
using ZonaActual = NSSituacion2.ZonaActual;
using UnityEngine.UI;
using NSTraduccionIdiomas;

namespace NSInterfaz
{
    public class PanelInterfazGeneralSituacion4 : AbstractSingletonPanelUIAnimation<PanelInterfazGeneralSituacion2>
    {
        [SerializeField] private ControladorSituacion4 refControladorSituacion4;

        [SerializeField] private ActiveZonesController refActiveZonesController;

        [SerializeField] private PanelInterfazSoloAnimacion refPanelInterfazSoloAnimacionSenializacionSeguridad;

        [SerializeField] private PanelInterfazSoloAnimacion refPanelInterfazSoloAnimacionHerramientasAlturas;
        [SerializeField] private clsRegistroDatosSituacion4 registrodeDatosPractica4;

        [SerializeField]
        private PanelGrafica refPanlGrafica;

        [SerializeField]
        private PanelPostePn refPanelPostePn;

        [SerializeField]
        private PanelPostePn refTrafo;

        [SerializeField]
        private PanelMenuPostePn refPanelMenuPostePn;

        [SerializeField]
        private Button BotnGraficaDeltrafo;


        #region MtdPublic

        public void OnButtonSenializacionAlturas()
        {
            if (refControladorSituacion4._avatarVestido)
                refPanelInterfazSoloAnimacionSenializacionSeguridad.Mostrar(true);
            else
                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("TextElementosDeSeguridad"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));

        }

        public void OnButtonHerramientaAlturas()
        {
            if (!refControladorSituacion4._avatarVestido)
            {
                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("TextElementosDeSeguridad"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                return;
            }

            refPanelInterfazSoloAnimacionHerramientasAlturas.Mostrar(true);

        }

        public void OnButtonReiniciarSituacion()
        {
            BoxMessageManager._instance.MtdCreateBoxMessageDecision(DiccionarioIdiomas._instance.Traducir("TextPreguntaReiniciar"), DiccionarioIdiomas._instance.Traducir("TextCancelar"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"), RestartSituationConfirm, CancelDecision);
        }

        public void OnButtonReturnBackSituation()
        {
            BoxMessageManager._instance.MtdCreateBoxMessageDecision(DiccionarioIdiomas._instance.Traducir("textDeseasAbandonar"), DiccionarioIdiomas._instance.Traducir("TextCancelar"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"), ConfirmReturnBackSituation, CancelDecision);
        }

        public void MtdpanelTrafoActivo(bool IsActivo)
        {
            BotnGraficaDeltrafo.interactable=  IsActivo;

        }

        public void onButtonPanelMenuPostePn()
        {
            if (refControladorSituacion4._avatarVestido) {

                refPanelMenuPostePn.Mostrar(true);
            }
            else
                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("TextElemtosDeProteccionNoSeleccionados"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));

        }

        public void onButtonPanelPostePn()
        {
            if (refControladorSituacion4._avatarVestido) {
                if (refControladorSituacion4._ImplemntosSeñalizacion)
                {
                    if (refControladorSituacion4._implementosAlturaColocados) {
                        if (registrodeDatosPractica4.inputTensionTransformador[1].text != "")
                        {
                            if (registrodeDatosPractica4.inputTransformadorPn[3].text != "")
                            {
                                refPanelPostePn.Mostrar(true);
                            }
                            else
                            {
                                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("TextDebeIngresarValorFusible"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                            }

                        }
                        else
                        {
                            BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("textValorDepotenciaTrafo"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                        }
                        
                    }
                    else
                    {
                        BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("textELementosTrabajodeALturaNoselect"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                    }

                }
                else
                {
                    BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("TextElementosDeSeñalizacion"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                }

            } else
                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("TextElementosDeSeguridad"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));

        }

        public void onButtonrTrafo()
        {
            if (refControladorSituacion4._avatarVestido) {
                if (refControladorSituacion4._ImplemntosSeñalizacion)
                {
                    refTrafo.Mostrar(true);
                }
                else {
                    BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("TextElementosDeSeñalizacion"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                }
            }else
                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("TextElementosDeSeguridad"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));

        }

        public void ZonasPractiva4()
        {
            refPanelPostePn.Mostrar(true);
            refTrafo.Mostrar(true);
        }
        #endregion

        #region MtdPrivate 

        private void RestartSituationConfirm()
        {
            SceneManager.LoadScene(4);
        }

        private void ConfirmReturnBackSituation()
        {
            SceneManager.LoadScene(0);
        }

        private void CancelDecision()
        {
            refActiveZonesController.ActiveAllZones();
        }

        #endregion


    }
}