﻿using System;
using System.Collections;
using System.Collections.Generic;
using NSActiveZones;
using NSBoxMessage;
using NSSituacion2;
using NSSituacion3;
using NSTraduccionIdiomas;
using UnityEngine;
using UnityEngine.SceneManagement;
using ZonaActual = NSSituacion2.ZonaActual;
using NSMenuSituationSelection;

namespace NSInterfaz
{
    public class PanelInterfazGeneralSituacion3 : AbstractSingletonPanelUIAnimation<PanelInterfazGeneralSituacion3>
    {
        #region members
        
        [SerializeField] private ControladorSituacion3 refControladorSituacion3;

        [SerializeField] private ActiveZonesController refActiveZonesController;

        [SerializeField] private PanelInterfazSoloAnimacion refPanelInterfazSoloAnimacionSenializacionSeguridad;

        [SerializeField] private PanelInterfazSoloAnimacion refPanelInterfazSoloAnimacionHerramientasAlturas;

        [SerializeField] private CanvasGroup canvasGroupBotonesDerecha;
        
        [SerializeField] private CanvasGroup canvasGroupBotonesIzquierda;
        
        #endregion
        
        private void Awake()
        {
    
        }

        public void OnButtonSenializacionAlturas()
        {
            if (refControladorSituacion3._avatarVestido)
                refPanelInterfazSoloAnimacionSenializacionSeguridad.Mostrar();
            else
                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeNoProteccionPersonal"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
        }

        public void OnButtonHerramientaAlturas()
        {
            if (!refControladorSituacion3._avatarVestido)
            {
                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeNoProteccionPersonal"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                return;
            }

            refPanelInterfazSoloAnimacionHerramientasAlturas.Mostrar();
        }

        public void OnButtonReiniciarSituacion()
        {
            BoxMessageManager._instance.MtdCreateBoxMessageDecision(DiccionarioIdiomas._instance.Traducir("mensajeReiniciarPractica"), DiccionarioIdiomas._instance.Traducir("TextCancelar"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"), RestartSituationConfirm, CancelDecision);
        }

        public void OnButtonReturnBackSituation()
        {
            BoxMessageManager._instance.MtdCreateBoxMessageDecision(DiccionarioIdiomas._instance.Traducir("mensajeAbandonarPractica"), DiccionarioIdiomas._instance.Traducir("TextCancelar"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"), ConfirmReturnBackSituation, CancelDecision);
        }

        private void RestartSituationConfirm()
        {
            GameObject.FindGameObjectWithTag("canvasMenuIni").GetComponent<SeleccionSituacion>().activarBienvenida();
        }

        private void ConfirmReturnBackSituation()
        {
            SceneManager.LoadScene(0);
        }

        private void CancelDecision()
        {
            refActiveZonesController.ActiveAllZones();
        }

        public void ActivarBotonesDerecha(bool argActivar = true)
        {
            canvasGroupBotonesDerecha.interactable = argActivar;
        }
        public void ActivarBotonesIzquierda(bool argActivar = true)
        {
            canvasGroupBotonesIzquierda.interactable = argActivar;
        }
        public void activarMensajeSituacion3()
        {
            BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeInicioSituacion3"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
        }
    }
}