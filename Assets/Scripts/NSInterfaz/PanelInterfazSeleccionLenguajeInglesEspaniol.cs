﻿using NSAvancedUI;
using NSTraduccionIdiomas;

namespace NSInterfaz
{
    public class PanelInterfazSeleccionLenguajeInglesEspaniol : AbstractPanelUIAnimation
    { 
        #region public methods

        /// <summary>
        /// cambia el idioma del simulador a español
        /// </summary>
        public void mtdTraducirEspañol()
        {
            DiccionarioIdiomas._instance.SetIdioma(ListaIdiomas.espaniol);
        }

        /// <summary>
        /// ccmabia el idioma del simmulador a ingles
        /// </summary>
        public void mtdTraducirIngles()
        {
            DiccionarioIdiomas._instance.SetIdioma(ListaIdiomas.ingles);
        }
        #endregion
    }
}