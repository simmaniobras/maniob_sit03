﻿using UnityEngine;

namespace NSInterfaz
{
    public class ButtonWearAvatar : MonoBehaviour
    {
        [SerializeField]
        private GameObject avatarForCopy;

        [SerializeField]
        private float scale;
        
        private void OnEnable()
        {
            UpdateAvatarOnButton();
        }

        public void OnButtonWearAvatar()
        {
            PanelInterfazWearAvatar._instance.Mostrar();
        }

        public void UpdateAvatarOnButton()
        {
            if (transform.childCount > 1)
                Destroy(transform.GetChild(1).gameObject);

            var tmpAvatarCopy = Instantiate(avatarForCopy, transform);
            tmpAvatarCopy.transform.localScale = new Vector3(scale, scale, 1f);
        }
    }
}